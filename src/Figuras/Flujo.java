/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Figuras;
import javafx.scene.Group;
import javafx.scene.shape.Line;

/**
 *
 * @author Nicolas
 */

public class Flujo {
    int indicePartidaLista;
    int indiceLlegadaLista;
    double x1=0,x2=0,y1=0,y2=0;
    Group nuevo;
    boolean cierra =false;
    boolean ciclo =false;
    public Flujo() {
    }
    
    public Group Dibujar(double x1, double y1, double x2, double y2){
        nuevo = new Group();
        this.x1=x1;
        this.x2=x2;
        this.y1=y1;
        this.y2 =y2-10;
        Line nueva = new Line(x1, y1, x2, y2-10);
        Line nueva2 = new Line(x2, y2, x2-10, y2-10);
        Line nueva3 = new Line(x2, y2, x2+10, y2-10);
        Line nueva4 = new Line(x2-10, y2-10, x2+10, y2-10);        
        nuevo.getChildren().addAll(nueva,nueva2,nueva3,nueva4);
        return nuevo;
           
    }

    public boolean isCierra() {
        return cierra;
    }

    public void setCierra(boolean cierra) {
        this.cierra = cierra;
    }

    public double getX1() {
        return x1;
    }

    public double getX2() {
        return x2;
    }

    public double getY1() {
        return y1;
    }

    public double getY2() {
        return y2;
    }

    public Group Dibujar2(double x1, double y1, double x2, double y2){
        nuevo = new Group();
        Line nueva = new Line(x1, y1, x1, y1+70);
        Line nueva2 = new Line(x1, y1+70, x2, y2);        
        nuevo.getChildren().addAll(nueva,nueva2);
        cierra =true;
        return nuevo;
           
    }

    public boolean isCiclo() {
        return ciclo;
    }

    public void setCiclo(boolean ciclo) {
        this.ciclo = ciclo;
    }
    public Group Dibujar3(double x1, double y1, double x2, double y2){
        nuevo = new Group();
        Line nueva = new Line(x1-50, y1, x1-100, y1);
        Line nueva2 = new Line(x1-100, y1, x2-100, y2);        
        Line nueva3 = new Line(x2-100, y2, x2, y2);
        nuevo.getChildren().addAll(nueva,nueva2,nueva3);
        ciclo =true;
        return nuevo;
           
    }

    
    public int getIndicePartidaLista() {
        return indicePartidaLista;
    }

    public Group getNuevo() {
        return nuevo;
    }

    public void setIndicePartidaLista(int indicePartidaLista) {
        this.indicePartidaLista = indicePartidaLista;
    }

    public int getIndiceLlegadaLista() {
        return indiceLlegadaLista;
    }

    public void setIndiceLlegadaLista(int indiceLlegadaLista) {
        this.indiceLlegadaLista = indiceLlegadaLista;
    }
    
}
