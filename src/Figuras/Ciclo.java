/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Figuras;
import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

/**
/**
 *
 * @author Nicolas
 */
public class Ciclo extends Figuras{
    private double valor, xmedio,ymedio;
    boolean unidoizq=false;
    int izq;

    public boolean isUnidoizq() {
        return unidoizq;
    }

    public void setUnidoizq(boolean unidoizq) {
        this.unidoizq = unidoizq;
    }

    public int getIzq() {
        return izq;
    }

    public void setIzq(int izq) {
        this.izq = izq;
    }

   
    
   
    @Override
    public Group Dibujar(double x1, double y1, String nom, Color color) {
        setColorFig(color);
        String idFigura = "ciclo";
        nuevo = new Group();
        double largoString = (nom.length()*5)/2;
        valor = largoString;
        /*LINEAS*/
        Line nueva = new Line(x1-50-largoString, y1-1, x1+50+largoString, y1-1);
        Line nueva1 = new Line(x1, y1-largoString, x1-50-largoString, y1+50);
        Line nueva2 = new Line(x1, y1-largoString, x1+50+largoString, y1+50);
        Line nueva3 = new Line(x1-50-largoString, y1+50, x1, y1+100+largoString);
        Line nueva4 = new Line(x1+50+largoString, y1+50, x1, y1+100+largoString);
        Line nueva5 = new Line(x1-50-largoString, y1+101+largoString, x1+50+largoString, y1+101+largoString);
        //Line nueva6 = new Line(x1+50+largoString, y1+50, x1+50+largoString+150, y1+50);
        /*COLOR LINEAS*/
        nueva.setStrokeWidth(2);
        nueva1.setStrokeWidth(2);
        nueva2.setStrokeWidth(2);
        nueva3.setStrokeWidth(2);
        nueva4.setStrokeWidth(2);
        nueva.setStroke(Color.WHITE);
        nueva1.setStroke(color);
        nueva2.setStroke(color);
        nueva3.setStroke(color);
        nueva4.setStroke(color);
        nueva5.setStroke(Color.WHITE);
        /*LINEA RELLENO*/
        Group relleno = new Group();
        relleno = creartriangulos(x1,y1-largoString,x1-50-largoString, y1+49, x1+50+largoString, y1+49,relleno, color);
        Group relleno2 = new Group();
        relleno2 = creartriangulos2(x1,y1+100+largoString,x1-50-largoString, y1+50, x1+50+largoString, y1+50,relleno2, color);
        
        /*for (int i = 50; i >= 0; i--) {
            Line nueva1Aux = new Line(x1, y1, x1-i-largoString, y1+50);
            Line nueva2Aux = new Line(x1, y1, x1+i+largoString, y1+50);
            Line nueva3Aux = new Line(x1-i-largoString, y1+50, x1+i+largoString, y1+50);
            nueva1Aux.setStroke(Color.LIGHTSKYBLUE);
            nueva2Aux.setStroke(Color.LIGHTSKYBLUE);
            nueva3Aux.setStroke(Color.LIGHTSKYBLUE);
            relleno.getChildren().addAll(nueva1Aux, nueva2Aux, nueva3Aux);
            
        }*/
        Label label = new Label(nom);
        label.setLayoutX(x1-40);
        label.setLayoutY((y1+y1+50)/2);
        label.setTextFill(Color.BLACK);
        label.setStyle("-fx-background-color: transparent;");
        Label label2 = new Label("6");
        label2.setTextFill(Color.WHITE);
        nuevo.getChildren().addAll(nueva, label2, nueva1, relleno,relleno2, nueva2, nueva3, nueva4,  label, nueva5);
        nuevo.setId(idFigura);
        return nuevo;
    }
    private Group creartriangulos(double x1, double y1, double x2,double y2, double x3,double y3, Group grupo, Color color){
	if((x1==x2 && x1==x3) ||(x1==x2 && y1==y3) ||(y1==y2 && x1==x3) || (y1==y2 && y1==y3)){
            return grupo;
        }
        else{
            Line nueva1Aux = new Line(x1, y1, x2, y2);
            Line nueva2Aux = new Line(x1, y1, x3, y3);
            Line nueva3Aux = new Line(x2, y2, x3, y3);
            nueva1Aux.setStroke(color);
            nueva2Aux.setStroke(color);
            nueva3Aux.setStroke(color);
            grupo.getChildren().addAll(nueva1Aux,nueva2Aux,nueva3Aux);
            creartriangulos(x1,y1,x2+1,y2-1,x3-1,y3-1,grupo, color);
        }
        return grupo;
    }
    private Group creartriangulos2(double x1, double y1, double x2,double y2, double x3,double y3,  Group grupo, Color color){
	if((x1==x2 && x1==x3) ||(x1==x2 && y1==y3) ||(y1==y2 && x1==x3) || (y1==y2 && y1==y3)){
            return grupo;
        }
        else{
            Line nueva1Aux = new Line(x1, y1, x2, y2);
            Line nueva2Aux = new Line(x1, y1, x3, y3);
            Line nueva3Aux = new Line(x2, y2, x3, y3);
            nueva1Aux.setStroke(color);
            nueva2Aux.setStroke(color);
            nueva3Aux.setStroke(color);
            grupo.getChildren().addAll(nueva1Aux,nueva2Aux,nueva3Aux);
            creartriangulos2(x1,y1,x2+1,y2+1,x3-1,y3+1,grupo, color);
        }
        return grupo;
    }
    
    @Override
    public Pane rellenarColor(double x1, double y1, Color color, double i) {
        Pane fondo = new Pane();  
        
        return fondo;
    }    

    public double getXmedio() {
        return xmedio;
    }

    public void setXmedio(double xmedio) {
        this.xmedio = xmedio;
    }

    public double getYmedio() {
        return ymedio;
    }

    public void setYmedio(double ymedio) {
        this.ymedio = ymedio;
    }

}
