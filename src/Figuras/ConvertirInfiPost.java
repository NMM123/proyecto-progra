/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Figuras;
import Clases.Funciones;
import java.util.Stack;

/**
 *
 * @author eduardovalenzuela
 */
public class ConvertirInfiPost<T> {
    
    public String convertirAPostFija(String entrada){
        String expresion = depurar(entrada);
        String [] expresionInfija = expresion.split(" ");
        
        Stack < String > E = new Stack <  > (); //Pila entrada
        Stack < String > P = new Stack <  > (); //Pila temporal para operadores
        Stack < String > S = new Stack <  > (); //Pila salida
        
        for (int i = expresionInfija.length - 1; i >= 0; i--) {
            E.push(expresionInfija[i]);
        }
        
        while (!E.isEmpty()) {
            switch (pref(E.peek())){
                case 1:
                    P.push(E.pop());
                    break;
                case 3:
                case 4:
                    while(pref(P.peek()) >= pref(E.peek())) {
                        S.push(P.pop());
                    }
                    P.push(E.pop());
                    break; 
                case 2:
                    while(!P.peek().equals("(")) {
                    S.push(P.pop());
                    }
                    P.pop();
                    E.pop();
                    break; 
                default:
                S.push(E.pop()); 
            } 
        }
        
        String postfix = S.toString().replaceAll("[\\]\\[,]", "");
        return postfix;
    }
    
    public String resolver(String entrada){
        entrada = convertirAPostFija(entrada);
        String[] postFija = entrada.split(" "); 
        
        Stack < String > E = new Stack <  > (); //Pila entrada
        Stack < String > P = new Stack <  > (); //Pila de operandos
        
        for (int i = postFija.length - 1; i >= 0; i--) {
            E.push(postFija[i]);
        }
        
        String operadores = "+-*/%"; 
        while (!E.isEmpty()) {
          if (operadores.contains("" + E.peek())) {
            P.push(evaluar(E.pop(), P.pop(), P.pop()) + "");
          }else {
            P.push(E.pop());
          } 
        }
        
        return P.peek();
        
        
    }
    
    public String resolverString(String entrada){
        entrada = convertirAPostFija(entrada);
        String[] postFija = entrada.split(" "); 
        
        Stack < String > E = new Stack <  > (); //Pila entrada
        Stack < String > P = new Stack <  > (); //Pila de operandos
        
        for (int i = postFija.length - 1; i >= 0; i--) {
            E.push(postFija[i]);
        }
        
        String operadores = "+"; 
        while (!E.isEmpty()) {
          if (operadores.contains("" + E.peek())) {
            P.push(evaluarString(E.pop(), P.pop(), P.pop()));
          }else {
            P.push(E.pop());
          } 
        }
        
        return P.peek();
        
        
    }

  private double evaluar(String op, String n2, String n1) {
    if (Funciones.EsNumero(n1) && Funciones.EsNumero(n2) ) {
        double num1 = Double.parseDouble(n1);
        double num2 = Double.parseDouble(n2);
        if (op.equals("+")) return (num1 + num2);
        if (op.equals("-")) return (num1 - num2);
        if (op.equals("*")) return (num1 * num2);
        if (op.equals("/")) return (num1 / num2);
        if (op.equals("%")) return (num1 % num2);
    }
    
    return 0;
  }
  
  private String evaluarString(String op, String n2, String n1) {
    return " "+n1+" "+n2;

  }

  //Depurar expresión algebraica
  private String depurar(String s) {
    s = s.replaceAll("\\s+", ""); //Elimina espacios en blanco
    s = "(" + s + ")";
    String simbols = "+-*/()";
    String str = "";
  
    //Deja espacios entre operadores
    for (int i = 0; i < s.length(); i++) {
      if (simbols.contains("" + s.charAt(i))) {
        str += " " + s.charAt(i) + " ";
      }else str += s.charAt(i);
    }
    return str.replaceAll("\\s+", " ").trim();
  } 

  //Jerarquia de los operadores
  private int pref(String op) {
    int prf = 99;
    if (op.equals("^")) prf = 5;
    if (op.equals("*") || op.equals("/")) prf = 4;
    if (op.equals("+") || op.equals("-")) prf = 3;
    if (op.equals(")")) prf = 2;
    if (op.equals("(")) prf = 1;
    return prf;
  }
}
