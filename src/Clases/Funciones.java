
package Clases;

import Figuras.Condicion;
import Figuras.ConvertirInfiPost;
import Figuras.Figuras;
import Figuras.GuardarVariable;
import java.util.ArrayList;
import javafx.scene.control.Alert;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Esta clase contiene todos los metodos auxiliares y necesarios
 * para realizar algunos procedimientos del controler.
 */
public class Funciones {
    
    /**
     * Esta funcion verifica si la cadena ingresada
     * se puede convertir a numero o no.
     * @param cadena
     * @return 
     */
    public static boolean EsNumero(String cadena) {

        boolean resultado;

        try {
            Double.parseDouble(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }
    
    
    /**
     * Esta funcion verifica si el caracter ingresado
     * se puede convertir a numero o no.
     * @param c
     * @return 
     */
    private static boolean EsNumero2(char c) {

        boolean resultado;

        try {
            Double.parseDouble(String.valueOf(c));
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }
    
    /**
     * Esta funcion verifica si el texto ingresado en la entrada 
     * esta correcto o no.
     * @param cadena
     * @return 
     */
    public static boolean VerificacionTextoEntrada(String cadena) {
        int contador=0;
        for (int i = 0; i < cadena.length(); i++) {
            char letraAux = cadena.charAt(i);
            if (letraAux == '+' || letraAux == '-' || letraAux == '*' || letraAux == '/') {
                return false;
            }
            if (letraAux == '=') {
                contador+=1;
            }
            
        }
        if (contador>1 || contador==0) {
            return false;
        }
        
        return true;
    }
    
    
    /**
     * Esta funcion verifica si el texto ingresado en el proceso
     * esta corrcto o no.
     * @param cadena
     * @return 
     */
    public static boolean VerificacionTextoProceso(String cadena) {
        int contador = 0;
        for (int i = 0; i < cadena.length(); i++) {
            char letraAux = cadena.charAt(i);
            if (letraAux == '=') {
                contador+=1;
            }
            
        }
        if (contador>1 || contador==0) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Esta funcion reviza si en la cadena se encuentra una coma para saber
     * que sea una salida.
     * @param cadena
     * @return 
     */
    public static boolean VerEntradaSalida(String cadena) {
        return cadena.contains(",");
    }
    
    
    /**
     * Esta funcion busca si un objeto se encuentra en el arreglo o no.
     * @param cadena
     * @param datosConsola
     * @return 
     */
    public static boolean buscarVariable(String cadena, ArrayList<GuardarVariable> datosConsola){
        for (int i = 0; i < datosConsola.size(); i++) {
            if (cadena == null ? datosConsola.get(i).getNom() == null : cadena.equals(datosConsola.get(i).getNom())) {
                return true;
            }
        }
        return false;
    }

    
    
    /**
     * Esta funcion buscar una cadena en el arreglo y retorna el valor de la variable. 
     * @param cadena
     * @param datosConsola
     * @return 
     */
    public static String buscar(String cadena, ArrayList<GuardarVariable> datosConsola){
        
        for (int i = 0; i < datosConsola.size(); i++) {
            if (cadena == null ? datosConsola.get(i).getNom() == null : cadena.equals(datosConsola.get(i).getNom())) {
                return datosConsola.get(i).getValor();
            }
        }
        return "";
    }
    
    /**
     * Esta funcion reemplaza las variables que se encuentran en la cadena por 
     * los valores asociados a cada variable.
     * @param cadena
     * @param datosConsola
     * @return 
     */
    public static String reemplazarVariables(String cadena, ArrayList<GuardarVariable> datosConsola){
        String finall = "";
        String[] partesStr;
        partesStr = cadena.split("\\%|\\/|\\*|\\-|\\+");
        
        
        for (int i = 0; i < partesStr.length; i++) {
            if (EsNumero(partesStr[i])) {
                
            }
            else{
                partesStr[i] = buscar(partesStr[i], datosConsola);
            }
            
        }
        int j=0;
        for (int i = 0; i < cadena.length(); i++) {
            char c= cadena.charAt(i);
            if (j==0) {
                finall=partesStr[j];
                j+=1;
            }
            if (c=='%' && EsNumero(partesStr[j])) {
                finall = finall + "%" + partesStr[j];
                j+=1;
                
            }
            if (c=='/' && EsNumero(partesStr[j])) {
                finall = finall + "/" + partesStr[j];
                j+=1;
            }
            if (c=='*' && EsNumero(partesStr[j])) {
                finall = finall + "*" + partesStr[j];
                j+=1;
            }
            if (c=='-' && EsNumero(partesStr[j])) {
                finall = finall + "-" + partesStr[j];
                j+=1;
            }
            if (c=='+') {
                finall = finall + "+" + partesStr[j];
                j+=1;
            }
            
        }
        if ("".equals(finall)) {
            return cadena;
        }
        
        return finall;
        
        
    }
    
    public static String reemplazarVariablesCondicionesIndiv(String cadena, ArrayList<GuardarVariable> datosConsola){
        String parte1="";
        String [] parts;
        parts = cadena.split("<|>|<=|>=|==");
        if (cadena.contains("<")) {
            parte1="<";
        }
        if (cadena.contains("<=")) {
            parte1="<=";
        }
        if (cadena.contains(">")) {
            parte1=">";
        }
        if (cadena.contains(">=")) {
            parte1=">=";
        }
        if (cadena.contains("==")) {
            parte1="==";
        }
        
        for (int i = 0; i < datosConsola.size(); i++) {
            if (parts[0] == null ? datosConsola.get(i).getNom() == null : parts[0].equals(datosConsola.get(i).getNom())) {
                parts[0] = datosConsola.get(i).getValor();
                
            }
            if (parts[1] == null ? datosConsola.get(i).getNom() == null : parts[1].equals(datosConsola.get(i).getNom())) {
                parts[1] = datosConsola.get(i).getValor();
            }
        }
        
        
        return parts[0]+parte1+parts[1];
        
        
    }
    
    public static String reemplazarVariablesCondiciones(String cadena, ArrayList<GuardarVariable> datosConsola){
        ArrayList<String> condicionales = posicionesAND_OR(cadena);
        String resultado="";
        String [] parts;
        parts=cadena.split("and|or");
        
        if (condicionales.isEmpty()) {
            resultado = reemplazarVariablesCondicionesIndiv(cadena, datosConsola);
        }
        else{
            for (int i = 0; i < parts.length; i++) {
                parts[i] = reemplazarVariablesCondicionesIndiv(parts[i], datosConsola);
            }
            int j=0;
            for (int i = 0; i < parts.length-1; i++) {
                resultado=resultado+parts[i]+condicionales.get(j)+parts[i+1];
                j++;
            }
        }
        
        return resultado;
    }
    
    /**
     * Esta funcion revisa si en una cadena se tiene solo numeros
     * o si tiene solo letras. Si tiene ambas, manda la cantidad de numeros
     * que tiene para que despues se valide.
     * @param cadena
     * @return 0 o 1 o cantNumeros
     */
    public static int confirmarTextoProceso(String cadena){
        int cont1=0, cont2=0;
        for (int i = 0; i < cadena.length(); i++) {
            char caracter = cadena.charAt(i);
            
            if (caracter=='(') {
                cont2+=1;
            }
            if (caracter==')') {
                cont2+=1;
                
            }
            if (caracter=='/') {
                cont2+=1;
                
            }
            if (caracter=='*') {
                cont2+=1;
                
            }
            if (caracter=='+') {
                cont2+=1;
                
            }
            if (caracter=='-') {
                cont2+=1;
                
            }
            if (caracter=='.') {
                cont2+=1;
                
            }
            if (EsNumero2(caracter)) {
                cont1+=1;
            }
            
        }
        
        if (cadena.length()-cont2 == cont1) {
            return 1;
        }
        if (cadena.length()-cont2 > cont1 || cadena.length()-cont2 < cont1) {
            return cont1;
        }
        return 0;
        
    }
    
    
    /**
     * Esta funcion realiza las operaciones del proceso, es decir,
     * operaciones basicas de numeros y concatenacion de cadenas.
     * @param part0
     * @param part1
     * @param datosConsola 
     */
    public static void parseoProceso(String part0, String part1, ArrayList<GuardarVariable> datosConsola){
        ConvertirInfiPost nuevaConversion = new ConvertirInfiPost();
        boolean existencia=true;
        for (int i = 0; i < datosConsola.size(); i++) {
            if (datosConsola.get(i).getNom() == null ? part0 == null : datosConsola.get(i).getNom().equals(part0)) {
                existencia=false;
                part1 = reemplazarVariables(part1, datosConsola);
                int numTexto = confirmarTextoProceso(part1);
                
                switch (numTexto) {
                    case 1:
                        part1 = calcularExpresiones(part1);
                        datosConsola.get(i).setValor(part1);
                        break;
                    case 0:
                        part1 = nuevaConversion.resolverString(part1);
                        datosConsola.get(i).setValor(part1);
                        break;
                    default:
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("ERROR");
                        alert.setHeaderText("Ooops, existe un error.");
                        alert.setContentText("Deseas realizar una operacion entre cadenas y numeros. \n Esto NO se puede realizar");
                        alert.showAndWait();
                        break;
                }
            }
        }
        if (existencia) {
            part1 = reemplazarVariables(part1, datosConsola);
            
            int numTexto = confirmarTextoProceso(part1);
            GuardarVariable nueva;
            switch (numTexto) {
                case 1:
                    part1 = calcularExpresiones(part1);
                    nueva = new GuardarVariable(part0, part1);
                    datosConsola.add(nueva);
                    break;
                case 0:
                    part1 = nuevaConversion.resolverString(part1);
                    nueva = new GuardarVariable(part0, part1);
                    datosConsola.add(nueva);
                    break;
                default:
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("ERROR");
                    alert.setHeaderText("Ooops, existe un error.");
                    alert.setContentText("Deseas realizar una operacion entre cadenas y numeros. \n Esto NO se puede realizar");
                    alert.showAndWait();
                    break;
            }
            
            
        }
    }
    
    /**
     * Elimina los espacios en blanco de un arreglo.
     * @param arreglo
     * @return 
     */
    public static String[] eliminarEspaciosBlancoArreglo (String [] arreglo){
        String [] partesFinal = new String[arreglo.length];
        int j=0;
        for (int i = 0; i < arreglo.length; i++) {
            if ("".equals(arreglo[i])) {
                
            }
            else{
                partesFinal[j] = arreglo[i];
                j++;
            }
        }
        return partesFinal;
    }
    
    /**
     * Retorna valor maximo de X del arreglo.
     * @param lista
     * @return 
     */
    public static double valorMaximoXArreglo(ArrayList<Figuras> lista){
        double valorMaxX = 0;
        for (int i = 0; i < lista.size(); i++) {
            if (valorMaxX<lista.get(i).getX1()) {
                valorMaxX = lista.get(i).getX1();
            }
            
        }
        
        return valorMaxX;
        
    }
    
    /**
     * Retorna el valor maximo de Y del arreglo.
     * @param lista
     * @return 
     */
    public static double valorMaximoYArreglo(ArrayList<Figuras> lista){
        double valorMaxY = 0;
        for (int i = 0; i < lista.size(); i++) {
            if (valorMaxY<lista.get(i).getY1()) {
                valorMaxY = lista.get(i).getY1();
            }
            
        }
        
        return valorMaxY;
        
    }
    
    /**
     * Esta funcion devulve una figura del arreglo.
     * @param lista
     * @param valorX
     * @param valorY
     * @return 
     */
    
    public static Figuras devolverFigura(ArrayList<Figuras> lista, double valorX, double valorY){
        Figuras nueva = null;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) instanceof Condicion) {
                if (valorX>lista.get(i).getX1()-50 && valorX<lista.get(i).getX1()+50 && valorY>lista.get(i).getY1()
                        && valorY<lista.get(i).getY1()+100) {
                    nueva=lista.get(i);
                }
            }
            else{
                if (valorX>lista.get(i).getX1()-50 && valorX<lista.get(i).getX1()+50 && valorY>lista.get(i).getY1()
                        && valorY<lista.get(i).getY1()+50) {
                    nueva=lista.get(i);
                }
            }
            
        }
        
        return nueva;
        
    }
    
    /**
     * Busca una figura y retorna un indice.
     * @param lista
     * @param buscar
     * @return 
     */
    public static int buscarIndice(ArrayList<Figuras> lista, Figuras buscar){
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i) == buscar) {
                return i;
            }
            
        }
        
        return -1;
        
    }
    
    /**
     * Esta funcion retorna el valor del calculo de un string.
     * debe ir con puro valores.
     * @param resolver
     * @return 
     */
    public static String calcularExpresiones(String resolver){
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");


        try {
            Object operation = engine.eval(resolver);
            return operation.toString();
        } catch (ScriptException e) {
        }
        return null;
        
    } 
    
    
    /**
     * Esta funcion se encarga de verificar la condicion de un elemento de la condicion.
     * @param valor
     * @return 
     */
    public static boolean condicionIndividual(String valor) {
        valor = valor.replace(" ", "");
        if (valor.contains("<")) {
            String[] parts;
            parts = valor.split("<");
            if (EsNumero(parts[0]) && EsNumero(parts[1]) && Double.parseDouble(parts[0]) < Double.parseDouble(parts[1])) {
                return true;
            }
            if (!EsNumero(parts[0]) && !EsNumero(parts[1]) && parts[0].length() < parts[1].length()) {
                return true;
            }
        }
        if (valor.contains(">")) {
            String[] parts;
            parts = valor.split(">");
            if (EsNumero(parts[0]) && EsNumero(parts[1]) && Double.parseDouble(parts[0]) > Double.parseDouble(parts[1])) {
                return true;
            }
            if (!EsNumero(parts[0]) && !EsNumero(parts[1]) && parts[0].length() > parts[1].length()) {
                return true;
            }
        }
        if (valor.contains("<=")) {
            String[] parts;
            parts = valor.split("<=");
            if (EsNumero(parts[0]) && EsNumero(parts[1]) && Double.parseDouble(parts[0]) <= Double.parseDouble(parts[1])) {
                return true;
            }
            if (!EsNumero(parts[0]) && !EsNumero(parts[1]) && parts[0].length() <= parts[1].length()) {
                return true;
            }
        }
        if (valor.contains(">=")) {
            String[] parts;
            parts = valor.split(">=");
            if (EsNumero(parts[0]) && EsNumero(parts[1]) && Double.parseDouble(parts[0]) >= Double.parseDouble(parts[1])) {
                return true;
            }
            if (!EsNumero(parts[0]) && !EsNumero(parts[1]) && parts[0].length() >= parts[1].length()) {
                return true;
            }
        }
        if (valor.contains("==")) {
            String[] parts;
            parts = valor.split("==");
            if (EsNumero(parts[0]) && EsNumero(parts[1]) && Double.parseDouble(parts[0]) == Double.parseDouble(parts[1])) {
                return true;
            }
            if (!EsNumero(parts[0]) && !EsNumero(parts[1]) && parts[0].length() == parts[1].length()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Esta funcion se encarga de retornar un arreglo con las posiciones
     * que encuentra los and y los or en el string.
     * @param valor
     * @return 
     */
    public static ArrayList<String> posicionesAND_OR(String valor) {
        ArrayList<String> valoresFinales = new ArrayList<>();
        for (int i = 0; i < valor.length() - 3; i++) {
            char c = valor.charAt(i);
            char c1 = valor.charAt(i + 1);
            char c2 = valor.charAt(i + 2);
            if (c == 'a' && c1 == 'n' && c2 == 'd') {
                valoresFinales.add("and");
            }
            if (c == 'o' && c1 == 'r') {
                valoresFinales.add("or");
            }
        }
        return valoresFinales;
    }

    /**
     * Esta funcion se encarga de la condicion general, es decir,
     * cuando vine dada por n condiciones.
     * @param valor
     * @return 
     */
    public static boolean condicionGeneral(String valor) {
        ArrayList<String> valoresPocisiones = posicionesAND_OR(valor);
        valor = valor.replace(" ", "");
        String[] parts;
        parts = valor.split("and|or");
        boolean auxiliar = true;

        for (int i = 0; i < valoresPocisiones.size(); i++) {

            if ("and".equals(valoresPocisiones.get(i))) {
                boolean valorUno = condicionIndividual(parts[i]);
                boolean valorDos = condicionIndividual(parts[i + 1]);

                if (valorUno && valorDos) {
                    auxiliar = true;
                } else if (!valorUno && !valorDos) {
                    auxiliar = true;
                } else {
                    auxiliar = false;
                }
            }
            if ("or".equals(valoresPocisiones.get(i))) {
                boolean valorUno = condicionIndividual(parts[i]);
                boolean valorDos = condicionIndividual(parts[i + 1]);
                if (valorUno || valorDos) {
                    auxiliar = true;
                } else {
                    auxiliar = false;
                }

            }
        }
        if (valoresPocisiones.isEmpty()) {
            auxiliar = condicionIndividual(valor);
        }

        return auxiliar;
    }
    
    
}
