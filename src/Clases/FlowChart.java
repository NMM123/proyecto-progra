package Clases;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author viveros
 */
public class FlowChart extends Application {
    private static Stage primaryStage; 
    
    private void setPrimaryStage(Stage stage) {
        FlowChart.primaryStage = stage;
    }

    static public Stage getPrimaryStage() {
        return FlowChart.primaryStage;
    }
    @Override
    public void start(Stage stage) throws Exception {
        setPrimaryStage(primaryStage);
        Parent root = FXMLLoader.load(getClass().getResource("FlowChart.fxml"));
        Scene scene = new Scene(root);
        scene.getStylesheets().add(this.getClass().getResource("/CSS/Estilo.css").toExternalForm());
        stage.getIcons().add(new Image("Imagenes/Icono.png"));
        stage.setTitle("FlowChart");
        stage.setScene(scene);
        stage.setMinWidth(1201);
        stage.setMinHeight(697);
        stage.show();         
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
