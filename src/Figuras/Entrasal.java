
package Figuras;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

/**
 *
 * @author Nicolas
 */
public class Entrasal extends Figuras {
    String idFigura = "entrada";
    @Override
    public Group Dibujar(double x1, double y1, String nom, Color color){
        setColorFig(color);
        nuevo = new Group();
        Group aux = new Group();
        double largoString = (nom.length()*5)/2;
        double k=0;
        for (int i = 0; i < 50; i++) {
            Line nueva = new Line(x1-largoString-50-k, y1+i, x1+largoString+50-k, y1+i);

            nueva.setStroke(color); 
            nueva.setStrokeWidth(2);
            aux.getChildren().add(nueva);
            k+=0.4;
        }
        /*LINEAS*/
        Line nueva = new Line(x1-largoString-50, y1, x1+largoString+50, y1);            //Linea Superior
        Line nueva2 = new Line(x1-largoString-70, y1+50, x1+largoString+30, y1+50);     //Linea Inferior
        Line nueva3 = new Line(x1+largoString+50, y1, x1+largoString+30, y1+50);        //Linea Lateral Derecha
        Line nueva4 = new Line(x1-largoString-50, y1, x1-largoString-70, y1+50);        //Linea Lateral Izquierda
        /*COLOR Y ANCHO DE LINEAS*/
        nueva.setStrokeWidth(2);
        nueva2.setStrokeWidth(2);
        nueva3.setStrokeWidth(2);
        nueva4.setStrokeWidth(2);
        nueva.setStroke(color);
        nueva2.setStroke(color);
        nueva3.setStroke(color);
        nueva4.setStroke(color);
        Label label = new Label(nom);
        label.setLayoutX(x1-40);
        label.setLayoutY((y1+y1+50)/2);
        label.setTextFill(Color.WHITE);
        Label label2 = new Label("1");
        label2.setTextFill(Color.WHITE);
        nuevo.getChildren().addAll(nueva, label2, nueva3, aux ,nueva4, label, nueva2);
        nuevo.setId(idFigura);
        return nuevo;
    }

    @Override
    public Pane rellenarColor(double x1, double y1, Color color, double i) {
        /*ANCHO Y ALTURA DEL RECTANGULO (PITAGORAS)*/
        double ancho = Math.sqrt(Math.pow((x1+i + 50) - (x1-i - 50), 2) + Math.pow(y1 - y1, 2));
        double alto = Math.sqrt(Math.pow((x1+i + 50) - (x1+i + 50), 2) + Math.pow((y1 + 50) - y1, 2));
        /*COLOR DE FONDO*/
        Pane fondo = new Pane();
        fondo.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
        fondo.setPrefSize(ancho, alto);
        fondo.setLayoutX(x1-50-i);
        fondo.setLayoutY(y1);    
        
        return fondo;
    }
    
    
   
}
