
package Figuras;

/**
 * Esta clase es la encargada de guardar los datos de una variable.
 * En esta se guarda el nombre de la variabe y su valor.
 */
public class GuardarVariable {
    private String nom;
    private String valor;

    public GuardarVariable(String nom, String valor) {
        this.nom = nom;
        this.valor = valor;
    }

    public GuardarVariable() {
    }
    
    

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
    
}
