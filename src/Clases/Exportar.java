package Clases;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.javaws.Main;
import java.awt.image.BufferedImage;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Node;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 *
 * @author eduardovalenzuela
 */
public class Exportar {
    
    public static void exportarPNG(Stage theStage, double valorX, double valorY, Node canvas){
        FileChooser fileChooser = new FileChooser();
        
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("png files (*.png)", "*.png"));

        File file = fileChooser.showSaveDialog(theStage);
        BufferedImage renderedImage = SwingFXUtils.fromFXImage(crearWritableImage(canvas, valorX, valorY), null);
        
        if (file != null) {
            try {
                ImageIO.write(renderedImage, "png", file);
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void exportarJPG(Stage theStage, double valorX, double valorY, Node canvas) throws IOException{
        FileChooser fileChooser = new FileChooser();
        
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("jpg files (*.jpg)", "*.jpg"));
        
        File file = fileChooser.showSaveDialog(theStage);
        BufferedImage bufferedImage;
		
	try {
			
	  //Lee la imagen del file.
	  bufferedImage = SwingFXUtils.fromFXImage(crearWritableImage(canvas, valorX, valorY), null);

	  // Crear el fondo blanco.
	  BufferedImage newBufferedImage = new BufferedImage(bufferedImage.getWidth(),
			bufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
	  newBufferedImage.createGraphics().drawImage(bufferedImage, 0, 0, Color.WHITE, null);
	  //Sobreescribe el archivo
	  ImageIO.write(newBufferedImage, "jpg", file);
			
	} catch (IOException e) {

	  e.printStackTrace();

	}
    }
    public static void exportaratxt(ArrayList<String> arreglo) throws IOException{
        FileWriter writer = new FileWriter("pseudo.txt");
        for (int i = 0; i < arreglo.size(); i++) {
            writer.write(arreglo.get(i)+System.lineSeparator());
        }        
        writer.close();
    }
    @SuppressWarnings("empty-statement")
    public static void exportarAPDF(Stage theStage, double valorX, double valorY, Node cv) throws FileNotFoundException{
        Document documento = new Document();
        
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Exportar como PDF");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PDF", "*.pdf"));
        File file = fileChooser.showSaveDialog(theStage);
        String nombreArchivo = file.toString();

        try {
            PdfWriter.getInstance(documento, new FileOutputStream(nombreArchivo));
        } catch (DocumentException | FileNotFoundException e) {};
        
        insertarImagenAPDF(documento, crearWritableImage(cv, valorX, valorY));
        
    }
    
    private static WritableImage crearWritableImage(Node canvas, double valorCanvasX, double valorCanvasY) { //Diagrama diagrama,
        
        //Vertice medida = diagrama.verificarMedida(diagrama.cacularPuntosMaxVertices());
        WritableImage writableImage = new WritableImage((int) valorCanvasX, (int) valorCanvasY);
        canvas.snapshot(null, writableImage);
        
        return writableImage;
    }
    
    private static void insertarImagenAPDF(Document documento, 
            WritableImage writableImage) {
        
        documento.open();
        float ancho = documento.getPageSize().getWidth() - documento.leftMargin() - documento.rightMargin();
        float alto = documento.getPageSize().getHeight() - documento.topMargin() - documento.bottomMargin();
        
        Image  imagen = null;
        
        ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
        try {
            ImageIO.write( SwingFXUtils.fromFXImage(writableImage, null ), "png", byteOutput);
            imagen = Image.getInstance(byteOutput.toByteArray());
        } catch (BadElementException | IOException e) {
        }
       
       imagen.scaleToFit(ancho, alto);
        
        try {
            documento.add(imagen);
        } catch (DocumentException e) {
        }
        documento.close();
    }
    
    

            

        
    
}
