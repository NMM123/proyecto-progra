/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Figuras;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Line;

/**
/**
/**
 *
 * @author Nicolas
 */
public class Docu extends Figuras{
    @Override
    public Group Dibujar(double x1, double y1, String nom, Color color) {
        setColorFig(color);
        String idFigura = "documento";
        nuevo = new Group();
        double largoString = (nom.length()*5)/2;
        /*LINEAS*/
        Line nueva = new Line(x1-50-largoString, y1, x1+50+largoString, y1);
        Line nueva2 = new Line(x1-50-largoString, y1, x1-50-largoString, y1+50);
        Line nueva3 = new Line(x1+50+largoString, y1, x1+50+largoString, y1+50);
        /*COLOR LINEAS*/
        nueva.setStrokeWidth(2);
        nueva2.setStrokeWidth(2);
        nueva3.setStrokeWidth(2);
        nueva.setStroke(color);
        nueva2.setStroke(color);
        nueva3.setStroke(color);
        /*CURVA*/
        CubicCurve cubicCurve = new CubicCurve(); 
        cubicCurve.setStartX(x1-50-largoString); 
        cubicCurve.setStartY(y1+50); 
        cubicCurve.setControlX1(x1-40-largoString); 
        cubicCurve.setControlY1(y1+80); 
        cubicCurve.setControlX2(x1+40+largoString); 
        cubicCurve.setControlY2(y1+30); 
        cubicCurve.setEndX(x1+50+largoString); 
        cubicCurve.setEndY(y1+50); 
        cubicCurve.setFill(color);
        cubicCurve.setStrokeWidth(2);
        cubicCurve.setStroke(Color.WHITE);
        Line nueva4 = new Line(x1-50-largoString, y1+61, x1+50+largoString, y1+61);
        nueva4.setStroke(Color.WHITE);
        Group aux = new Group();
        int k=0;
        for (int i = 39; i < 50; i++) {
            Line nuevaLinea = new Line(x1-50-largoString, y1+i, x1+largoString+40-k, y1+i);
            nuevaLinea.setStroke(color);
            nuevaLinea.setStrokeWidth(2);
            aux.getChildren().add(nuevaLinea);
            k++;
        }
        Label label = new Label(nom);
        label.setLayoutX(x1-40);
        label.setLayoutY((y1+y1+50)/2);
        label.setTextFill(Color.WHITE);
        label.setStyle("-fx-background-color: transparent;");
        Pane nuevoPane = rellenarColor(x1, y1, color, largoString);
        Label label2 = new Label("3");
        label2.setTextFill(Color.WHITE);
        nuevo.getChildren().addAll(nueva, label2, cubicCurve, nuevoPane, aux, nueva2, nueva3, label, nueva4);
        nuevo.setId(idFigura);
        return nuevo;
    }

    @Override
    public Pane rellenarColor(double x1, double y1, Color color, double i) {
        /*ANCHO Y ALTURA DEL RECTANGULO (PITAGORAS)*/
        double ancho = Math.sqrt(Math.pow((x1+i + 50) - (x1-i - 50), 2) + Math.pow(y1 - y1, 2));
        double alto = Math.sqrt(Math.pow((x1+i + 50) - (x1+i + 50), 2) + Math.pow((y1 + 50) - y1, 2));
        /*COLOR DE FONDO*/
        Pane fondo = new Pane();
        fondo.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
        fondo.setPrefSize(ancho, alto);
        fondo.setLayoutX(x1-50-i);
        fondo.setLayoutY(y1);    
        
        return fondo;
    }
    
}
