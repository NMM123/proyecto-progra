package Figuras;

import javafx.scene.Group;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public abstract class Figuras{
    public abstract Group Dibujar(double x1, double y1, String nom, Color color);
    public abstract Pane rellenarColor(double x1, double y1, Color color, double i); 
    double x1,y1;
    String nom;
    Group nuevo;
    boolean unidosiguiente=false;
    boolean unidoanterior=false;
    int siguiente;
    Color colorFig;

    public Color getColorFig() {
        return colorFig;
    }

    public void setColorFig(Color colorFig) {
        this.colorFig = colorFig;
    }

    public void setNuevo(Group nuevo) {
        this.nuevo = nuevo;
    }
    
    

    public boolean isUnidosiguiente() {
        return unidosiguiente;
    }

    public Group getNuevo() {
        return nuevo;
    }
    
    public void setUnidosiguiente(boolean unidosiguiente) {
        this.unidosiguiente = unidosiguiente;
    }

    public boolean isUnidoanterior() {
        return unidoanterior;
    }

    public void setUnidoanterior(boolean unidoanterior) {
        this.unidoanterior = unidoanterior;
    }

    public int getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(int siguiente) {
        this.siguiente = siguiente;
    }
    

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public double getY1() {
        return y1;
    }

    public void setY1(double y1) {
        this.y1 = y1;
    }
}
