/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;
import java.io.FileWriter;
import Figuras.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author viveros
 */
public class FlowChartController implements Initializable {

    @FXML
    private Pane controles;
    @FXML
    private Button linea;
    @FXML
    private Button etapa;
    @FXML
    private Button inicioFin;
    @FXML
    private Button entradaSalida;
    @FXML
    private Button documento;
    @FXML
    private Button decision;
    @FXML
    private Button ciclo;
    @FXML
    private Button juntarciclo;
    @FXML
    private Button eliminar;
    @FXML
    private Button eliminarElemento;
    @FXML
    private Pane consola;
    @FXML
    private ScrollPane scroll;
    @FXML
    private Pane mostrar;
    @FXML
    private Button ejecutar;
    @FXML
    private Button ejecutar2;
    @FXML
    private Button mover;
    @FXML
    private Button cortarhilo;
    @FXML
    private Button exportar;
    @FXML
    private Button cambiarNombre;
    @FXML
    private Button pintarBoton;
    @FXML
    private ColorPicker colorpicker;    
    @FXML
    private Button cambiarFigura;
    
    private int figura1,figura2;
    private double ejeX, ejeY,ejeX1,ejeY1; //EJE X Y EJE Y DONDE SE HACE CLIC
    private boolean seguirDibujando; //BANDERA QUE NOS AYUDA A SEGUIR O TERMINAR CON EL CLIC EN EL PANEL DEL DIAGRAMA
    private boolean inicioCil = true , finalCil = true;
    /*ARRAYLIST QUE GUARDA TODOS LOS ELEMENTOS DEL DIAGRAMA*/
    ArrayList<Group> diagrama = new ArrayList<>();
    /*ARRAYLIST QUE GUARDA TODAS LAS LINEAS DEL DIAGRAMA*/
    ArrayList<Flujo> lineas = new ArrayList<>();
    /*ARRAYLIST QUE GUARDA TODAS LAS FIGURAS DEL DIAGRAMA*/
    ArrayList<Figuras> figuras = new ArrayList<>();
    /*ARRAYLIST QUE GUARDA LOS DATOS DE LA CONSOLA*/
    ArrayList<GuardarVariable> datosConsola = new ArrayList<>();
    /*ARRAYLIST QUE GUARDA LAS CONDICIONES*/
    ArrayList<Condicion> condis = new ArrayList<>();
    /*TEXTFLOW QUE NOS SIRVE PARA AGREGAR TEXTO A LA CONSOLA*/
    TextFlow root = new TextFlow();
    /*STRING QUE NOS SIRVE PARA GUARDAR LOS DATOS A IMPRIMIR*/
    String textoAux = new String();
    /**/
    String textoCilindro;
    /*LETRA DE LA CONSOLA*/
    Font letra = new Font("Lucida Console", 15);    
    double valorxCanvas,valoryCanvas;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ImageView img1 = new ImageView("Imagenes/Linea.png");
        ImageView img2 = new ImageView("Imagenes/Inicio-Fin.png");
        ImageView img3 = new ImageView("Imagenes/Etapa.png");
        ImageView img4 = new ImageView("Imagenes/Entrada-Salida.png");
        ImageView img5 = new ImageView("Imagenes/Documento.png");
        ImageView img6 = new ImageView("Imagenes/Eliminar.png");
        ImageView img7 = new ImageView("Imagenes/EliminarElemento.png");
        ImageView img8 = new ImageView("Imagenes/Ejecutarpaso.png");
        ImageView img9 = new ImageView("Imagenes/Mover.png");
        ImageView img10 = new ImageView("Imagenes/Decision.png");
        ImageView img11 = new ImageView("Imagenes/Ejecutar.png");
        ImageView img12 = new ImageView("Imagenes/flechacondicion.png");
        ImageView img13 = new ImageView("Imagenes/Ciclo.png");
        ImageView img14 = new ImageView("Imagenes/flechaciclo.png");
        ImageView img15 = new ImageView("Imagenes/Exportar.png");
        ImageView img16 = new ImageView("Imagenes/rename.png");
        ImageView img17 = new ImageView("Imagenes/pintar.png");
        ImageView img18 = new ImageView("Imagenes/cambiarFig.png");
        

        linea.setGraphic(img1);
        entradaSalida.setGraphic(img4);
        inicioFin.setGraphic(img2);
        etapa.setGraphic(img3);
        documento.setGraphic(img5);
        eliminar.setGraphic(img6);
        eliminarElemento.setGraphic(img7);
        ejecutar.setGraphic(img8);
        mover.setGraphic(img9);
        ejecutar2.setGraphic(img11);
        decision.setGraphic(img10);
        cortarhilo.setGraphic(img12);
        ciclo.setGraphic(img13);
        juntarciclo.setGraphic(img14);
        exportar.setGraphic(img15);
        cambiarNombre.setGraphic(img16);
        pintarBoton.setGraphic(img17);
        cambiarFigura.setGraphic(img18);
        valorxCanvas = mostrar.getPrefHeight();
        valoryCanvas = mostrar.getPrefWidth();
        bloquearAlInicio(true);
        inicioFin.setDisable(false);
    } 
    private void bloquearAlFinal(){
        etapa.setDisable(true);
        inicioFin.setDisable(true);
        entradaSalida.setDisable(true);
        documento.setDisable(true);
        decision.setDisable(true);
        ciclo.setDisable(true);
        juntarciclo.setDisable(true);
        eliminarElemento.setDisable(true);
        mover.setDisable(true);
        //cortarhilo.setDisable(true);
        cambiarNombre.setDisable(true);
        cambiarFigura.setDisable(true);
        pintarBoton.setDisable(true);
    }
    
    public void bloquearAlInicio(boolean inicio) {
        if (inicio) {
            
            linea.setDisable(true);
            etapa.setDisable(true);
            inicioFin.setDisable(true);
            entradaSalida.setDisable(true);
            documento.setDisable(true);
            decision.setDisable(true);
            ciclo.setDisable(true);
            juntarciclo.setDisable(true);
            eliminar.setDisable(true);
            eliminarElemento.setDisable(true);
            ejecutar.setDisable(true);
            mover.setDisable(true);
            ejecutar2.setDisable(true);
            cortarhilo.setDisable(true);
            exportar.setDisable(true);
            cambiarNombre.setDisable(true);
            cambiarFigura.setDisable(true);
            pintarBoton.setDisable(true);
        }
        else {
            linea.setDisable(false);
            etapa.setDisable(false);
            inicioFin.setDisable(false);
            entradaSalida.setDisable(false);
            documento.setDisable(false);
            decision.setDisable(false);
            ciclo.setDisable(false);
            juntarciclo.setDisable(false);
            eliminar.setDisable(false);
            eliminarElemento.setDisable(false);
            ejecutar.setDisable(false);
            mover.setDisable(false);
            ejecutar2.setDisable(false);
            cortarhilo.setDisable(false);
            exportar.setDisable(false);
            cambiarNombre.setDisable(false);
            cambiarFigura.setDisable(false);
            pintarBoton.setDisable(false);
        }
    } 
    
    private void imprimirEnConsolaEntrada(String texto) {
        texto = texto.replace(" ", ""); //Borramos los espacios
        if (!"".equals(texto)) {
            textoAux = texto.replace("=", "<-"); //Cambiamos el = por el <-
            Text codigo = new Text(textoAux); //Pasamos el string que recibimos a texto
            codigo.setFill(Color.BLACK); codigo.setFont(letra);
            Text tab = new Text("\n"); //Agregamos una tabulacion

            root.getChildren().addAll(codigo, tab);
            consola.getChildren().add(root);
        }
       
    }    
    
    public boolean verificarFigura(double valorEjeY, double valorEjeX) {
        for (int i = 0; i < figuras.size(); i++) {
            if (figuras.get(i) instanceof Ciclo || figuras.get(i)  instanceof Condicion) {
                if (valorEjeY>=figuras.get(i).getY1() && valorEjeY<=figuras.get(i).getY1()+100 && valorEjeX>=figuras.get(i).getX1()-50 && valorEjeY<=figuras.get(i).getX1()+50) {
                    return false;
                }
            }
            else{
                if (valorEjeY>=figuras.get(i).getY1() && valorEjeY<=figuras.get(i).getY1()+50 && valorEjeX>=figuras.get(i).getX1()-50 && valorEjeY<=figuras.get(i).getX1()+50) {
                    return false;
                }
            }
        }
        return true;
    }
    
    
    public boolean verificarOrillas(double valorEjeX, double valorEjeY) {
        if (valorEjeX >= 0.0 && valorEjeX <= 50.0 || valorEjeY >= 0.0 && valorEjeY <= 10.0) {
            return false;
        }
        return true;
    }
    private boolean probarsiexiste(double x1,double x2,double y1, double y2){
        if (x1<x2+50 && x1>x2-50 && y1<y2+50 &&y1>y2-50) {
            return true;
        }
        return false;
    }
    /*CREA LAS LINEAS DEL DIAGRAMA*/
    @FXML
    private void linea(ActionEvent event) {
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        seguirDibujando = true;
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouse) {
                /*OBTENEMOS EL EJE X Y EL EJE Y DONDE SE HACE CLICK*/
                if (seguirDibujando) {
                    ejeX = mouse.getX();
                    ejeY = mouse.getY();
                    seguirDibujando = false;
                }
                for (int i = 0; i < figuras.size(); i++) {
                    if (probarsiexiste(ejeX,figuras.get(i).getX1(),ejeY,figuras.get(i).getY1())) {
                        unir(event,ejeX,ejeY);
                    }
                }
            }

        });
    }
    //Borrar cuando esten terminadas las otras PORFAVOR
    private void weaenblanco(){
        seguirDibujando = true;
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouse) {
                /*OBTENEMOS EL EJE X Y EL EJE Y DONDE SE HACE CLICK*/
                if (seguirDibujando) {
                    ejeX = mouse.getX();
                    ejeY = mouse.getY();
                    seguirDibujando = false;
                }                
            }
        });
    }
    
    private void unir(ActionEvent event, double x1, double y1){
        seguirDibujando = true;
        figura1=-1;        
        figura2=-1;
        for (int i = 0; i < figuras.size(); i++) {
                    if (probarsiexiste(x1,figuras.get(i).getX1(),y1,figuras.get(i).getY1())) {
                        figura1=i;                                               
                    }
                }
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            
            @Override
            public void handle(MouseEvent mouse) {
                /*OBTENEMOS EL EJE X Y EL EJE Y DONDE SE HACE CLICK*/
                if (seguirDibujando) {
                    ejeX = mouse.getX();
                    ejeY = mouse.getY();
                    seguirDibujando = false;
                }
                for (int i = 0; i < figuras.size(); i++) {
                    
                    if (probarsiexiste(ejeX,figuras.get(i).getX1(),ejeY,figuras.get(i).getY1())) {
                        figura2=i;               
                        
                    }
                }
                if (!figuras.get(figura1).isUnidosiguiente() && !figuras.get(figura2).isUnidoanterior() && figura1!=figura2) {
                    if (figuras.get(figura1) instanceof Condicion) {
                        Flujo f = new Flujo();
                        Group Dibujar = f.Dibujar(figuras.get(figura1).getX1(), figuras.get(figura1).getY1()+100, figuras.get(figura2).getX1(), figuras.get(figura2).getY1());
                        f.setIndicePartidaLista(figura1);
                        f.setIndiceLlegadaLista(figura2);
                        lineas.add(f);
                        mostrar.getChildren().add(Dibujar);                        
                        
                    }
                    else{
                        Flujo f = new Flujo();
                        Group Dibujar = f.Dibujar(figuras.get(figura1).getX1(), figuras.get(figura1).getY1()+50, figuras.get(figura2).getX1(), figuras.get(figura2).getY1());
                        f.setIndicePartidaLista(figura1);
                        f.setIndiceLlegadaLista(figura2);
                        lineas.add(f);
                        mostrar.getChildren().add(Dibujar);
                    }
                    figuras.get(figura1).setUnidosiguiente(true);
                    figuras.get(figura2).setUnidoanterior(true);
                    figuras.get(figura1).setSiguiente(figura2);
                    
                }
                else if (figuras.get(figura1) instanceof Condicion) {
                    if (!((Condicion)figuras.get(figura1)).isUnidoder() && !(figuras.get(figura1).getSiguiente()==figura2)) {
                        Flujo f = new Flujo();
                        Group Dibujar = f.Dibujar(figuras.get(figura1).getX1()+50, figuras.get(figura1).getY1()+50, figuras.get(figura2).getX1(), figuras.get(figura2).getY1());
                        lineas.add(f);
                        f.setIndicePartidaLista(figura1);
                        f.setIndiceLlegadaLista(figura2);
                        mostrar.getChildren().add(Dibujar);
                        ((Condicion)figuras.get(figura1)).setUnidoder(true);
                        ((Condicion)figuras.get(figura1)).setDer(figura2);
                    }
                }
            }

        });
    }
    private void restarelimina(int c){
        for (int i = 0; i < figuras.size(); i++) {
            if (figuras.get(i).getSiguiente()>=c) {
                figuras.get(i).setSiguiente(c-1);
            }
        }
        for (int i = 0; i < lineas.size(); i++) {
            if (lineas.get(i).getIndiceLlegadaLista()>=c) {
                lineas.get(i).setIndiceLlegadaLista(c-1);
            }
            if (lineas.get(i).getIndicePartidaLista()>=c) {
                lineas.get(i).setIndicePartidaLista(c-1);
            }
        }
    }
    private void eliminarLineas(int c){
        for (int i = 0; i < lineas.size(); i++) {
            if (lineas.get(i).getIndicePartidaLista()==c) {  
                int a=lineas.get(i).getIndiceLlegadaLista();
                int b =figuras.size();
                figuras.get(lineas.get(i).getIndiceLlegadaLista()).setUnidoanterior(false);
                lineas.remove(i);
                i-=1;
            }
            else if (lineas.get(i).getIndiceLlegadaLista()==c) {
                if (figuras.get(lineas.get(i).getIndicePartidaLista()) instanceof Condicion) {
                    if (((Condicion)figuras.get(lineas.get(i).getIndicePartidaLista())).getDer()==c) {
                        ((Condicion)figuras.get(lineas.get(i).getIndicePartidaLista())).setUnidoder(false);
                    }
                    else{
                        figuras.get(lineas.get(i).getIndicePartidaLista()).setUnidosiguiente(false);
                    }                    
                }
                else{
                    figuras.get(lineas.get(i).getIndicePartidaLista()).setUnidosiguiente(false);
                }               
               lineas.remove(i);
               i-=1; 
                
            }
        }
    }
    /*private void eliminarCondi(int c){
        if (((Condicion)figuras.get(c)).isUnidoder()) {
            eliminarSiguiente(((Condicion)figuras.get(c)).getDer());
            figuras.remove(c);
        }
    }
    private void eliminarSiguiente(int c){
        if (figuras.get(c).isUnidosiguiente()) {
            eliminarSiguiente(figuras.get(c).getSiguiente());
            figuras.remove(c);
        }
    }*/
    private void mostrarwas(){
        mostrar.getChildren().clear();
        for (int i = 0; i < figuras.size(); i++) {
            mostrar.getChildren().add(figuras.get(i).getNuevo());
        }
        for (int i = 0; i < lineas.size(); i++) {
            mostrar.getChildren().add(lineas.get(i).getNuevo());
        }
    }
    /*FIGURA QUE REPRESENTA EL PROCESO*/
    @FXML
    private void Rectangulo(ActionEvent event) {
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        seguirDibujando = true;
            
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent mouse) {
                TextInputDialog ventana = new TextInputDialog();
                ventana.setTitle("PROCESO");
                ventana.setHeaderText(null);
                ventana.setContentText("Ingrese un proceso: ");
                Optional<String> tProceso = ventana.showAndWait();
                if (tProceso.isPresent()) {
                    String textoProceso = tProceso.get();
                    /*OBTENEMOS EL EJE X Y EL EJE Y DONDE SE HACE CLICK*/
                    if (seguirDibujando) {
                        ejeX = mouse.getX();
                        ejeY = mouse.getY();
                        seguirDibujando = false;
                    }
                    //TAMBIEN DEBEMOS VERIFICAR QUE EL TEXTO INGRESADO SEA CORRECTO
                    if (seguirDibujando == false && verificarFigura(ejeY, ejeX) && verificarOrillas(ejeX, ejeY) && Funciones.VerificacionTextoProceso(textoProceso)) {
                        Proceso p = new Proceso();
                        Color colorFigura = buscarColorFigura("proceso", p);
                        Group nuevoProceso = p.Dibujar(ejeX, ejeY, textoProceso, colorFigura);                        
                        mostrar.getChildren().add(nuevoProceso);
                        p.setX1(ejeX);
                        p.setY1(ejeY);
                        p.setNom(textoProceso);
                        //ACA DEBEMOS AGREGAR FIGURA
                        figuras.add(p);
                        mostrar.setPrefWidth(mostrar.getPrefWidth()+500);
                        mostrar.setPrefHeight(mostrar.getPrefHeight()+500);
                        scroll.setContent(mostrar);
                        seguirDibujando = true;
                    }
                }

            }

        });
    }

    @FXML
    private void Cilindro(ActionEvent event) {
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        seguirDibujando = true;
        /*VERIFICAMOS QUE NO HAYA NINGUN INICIO NI FINAL*/
        if (!inicioCil && !finalCil) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("ERROR");
            alert.setHeaderText(null);
            alert.setContentText("El diagrama esta terminado, no puedes seguir agregando.");
            alert.showAndWait();            
        }
        else {
            mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouse) {
                    if (seguirDibujando) {
                        ejeX = mouse.getX();
                        ejeY = mouse.getY();
                        seguirDibujando = false;
                    }
                    if (seguirDibujando == false && verificarFinal2()<2) {
                        bloquearAlInicio(false);
                        Infin c = new Infin();
                        Group nuevoCilindro = new Group();
                        /*SI EL ELEMENTO A DIBUJAR ES UN INICIO*/
                        if (inicioCil) {
                            textoCilindro = "Inicio";
                            Color colorFigura = buscarColorFigura("inicio", c);
                            nuevoCilindro = c.Dibujar(mostrar.getPrefWidth() / 2, 50, textoCilindro, colorFigura);
                            inicioCil = false;
                            /*SI EL DIAGRAMA NO ESTA VACIO*/
                            if (diagrama.size() > 0 ) {
                                /*ASIGNAMOS LAS COORDENADAS A EL INICIO*/
                                c.setX1(mostrar.getPrefWidth() / 2);
                                c.setY1(50);
                                c.setNom(textoCilindro);
                                ArrayList<Group> auxDiagrama = new ArrayList<>();
                                auxDiagrama.add(nuevoCilindro);
                                ArrayList<Figuras> auxFigura = new ArrayList<>();
                                auxFigura.add(c);
                                for (int i = 0; i < diagrama.size(); i++) {
                                    auxDiagrama.add(diagrama.get(i));
                                }
                                for (int i = 0; i < figuras.size(); i++) {
                                    auxFigura.add(figuras.get(i));
                                }
                                diagrama.clear();
                                figuras.clear();
                                diagrama = auxDiagrama;
                                figuras = auxFigura;
                                nuevoCilindro=diagrama.get(0);
                                
                            }else{
                                diagrama.add(nuevoCilindro);
                                c.setX1(mostrar.getPrefWidth() / 2);
                                c.setY1(50);
                                c.setNom(textoCilindro);
                                figuras.add(c);
                                nuevoCilindro = diagrama.get(0);
                            }
                            
                        }
                        else if (finalCil &&  verificarFigura(ejeY, ejeX) && verificarOrillas(ejeX, ejeY)) {
                            Alert alert = new Alert(AlertType.CONFIRMATION);
                            alert.setTitle("INFORMACION");
                            alert.setHeaderText("¿Seguro que desea colocar el final?");
                            alert.setContentText("Si lo coloca no podra realizar nada más.");

                            Optional<ButtonType> result = alert.showAndWait();
                            if (result.get() == ButtonType.OK){
                                textoCilindro = "Final";
                                Color colorFigura = buscarColorFigura("fin", c);
                                nuevoCilindro = c.Dibujar(ejeX, ejeY, textoCilindro, colorFigura);                            
                                c.setX1(ejeX);
                                c.setY1(ejeY);
                                c.setNom(textoCilindro);

                                bloquearAlFinal();
                                finalCil = false;
                                //linea.setDisable(false);
                                //exportar.setDisable(false);
                                //eliminar.setDisable(false);
                                //eliminarElemento.setDisable(false);

                                figuras.add(c);
                                diagrama.add(nuevoCilindro);
                            }
                            
                            
                        }
                        mostrar.getChildren().addAll(nuevoCilindro);
                        seguirDibujando = true;
                    }
                }
            });            
        }
    }

    @FXML
    private void Trapecio(ActionEvent event) {
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        seguirDibujando = true;
        /*VENTANA EMERGENTE QUE NOS PERMITE INGRESAR UNA ENTRADA*/
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouse) {
                TextInputDialog ventana = new TextInputDialog();
                ventana.setTitle("ENTRADA");
                ventana.setHeaderText(null);
                ventana.setContentText("Ingrese una entrada: ");
                Optional<String> tEntrada = ventana.showAndWait();
                if (tEntrada.isPresent()) {
                    String textoEntrada = tEntrada.get();
                    if (seguirDibujando) {
                        ejeX = mouse.getX();
                        ejeY = mouse.getY();
                        seguirDibujando = false;
                    }

                    if (seguirDibujando == false && verificarOrillas(ejeX, ejeY) && verificarFigura(ejeY, ejeX)){
                        Entrasal e = new Entrasal();
                        Color colorFigura = buscarColorFigura("entrada", e);
                        Group nuevaEntrada = e.Dibujar(ejeX, ejeY, textoEntrada, colorFigura);
                        e.setX1(ejeX);
                        e.setY1(ejeY);
                        e.setNom(textoEntrada);

                        if (!Funciones.VerEntradaSalida(textoEntrada) && Funciones.VerificacionTextoEntrada(textoEntrada)) {
                            //agregarFigura(e);
                            figuras.add(e); 
                            mostrar.getChildren().add(nuevaEntrada);
                            //diagrama.add(nuevo); F
                            seguirDibujando = true;
                            consola.getChildren().clear(); //LIMPIAMOS LA CONSOLA ANTES PARA ACTUALIZARLA
                            imprimirEnConsolaEntrada(textoEntrada); //LO AGREGAMOS A LA CONSOLA  
                        }
                        else if (Funciones.VerEntradaSalida(textoEntrada)) {
                            //agregarFigura(e);
                            figuras.add(e); 
                            mostrar.getChildren().add(nuevaEntrada);
                            //diagrama.add(nuevo);
                            seguirDibujando = true;
                            consola.getChildren().clear(); //LIMPIAMOS LA CONSOLA ANTES PARA ACTUALIZARLA
                            //imprimirEnConsolaSalida(textoEntrada);
                        }
                        mostrar.setPrefWidth(mostrar.getPrefWidth()+500);
                        mostrar.setPrefHeight(mostrar.getPrefHeight()+500);
                        scroll.setContent(mostrar);
                    }
                }
            }
        });            
    }

    @FXML
    private void Documento(ActionEvent event) {
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        seguirDibujando = true;
        /*VENTANA EMERGENTE QUE NOS PERMITE INGRESAR UN DOCUMENTO*/
            
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouse) {
                TextInputDialog ventana = new TextInputDialog();
                ventana.setTitle("DOCUMENTO");
                ventana.setHeaderText(null);
                ventana.setContentText("Ingrese un documento: ");
                Optional<String> tDocu = ventana.showAndWait();        

                if (tDocu.isPresent()) {
                    String textoDocu = tDocu.get();
                    if (seguirDibujando) {
                        ejeX = mouse.getX();
                        ejeY = mouse.getY();
                        seguirDibujando = false;
                    }
                    if (!seguirDibujando && verificarOrillas(ejeX, ejeY) && verificarFigura(ejeY, ejeX)) {
                        Docu d = new Docu();
                        Color colorFigura = buscarColorFigura("documento", d);
                        Group nuevo = d.Dibujar(ejeX, ejeY, textoDocu, colorFigura);                        
                        d.setX1(ejeX);
                        d.setY1(ejeY);
                        d.setNom(textoDocu);
                        //agregarFigura(docu);
                        figuras.add(d); 
                        mostrar.getChildren().add(nuevo);
                        //diagrama.add(nuevo);
                        seguirDibujando = true; 
                    }
                    mostrar.setPrefWidth(mostrar.getPrefWidth()+500);
                    mostrar.setPrefHeight(mostrar.getPrefHeight()+500);
                    scroll.setContent(mostrar);
                }
            }
        });
    }

    @FXML
    private void Decision(ActionEvent event) {
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        seguirDibujando = true;
        /*VENTANA EMERGENTE QUE NOS PERMITE INGRESAR UNA CONDICION*/
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouse) {
                TextInputDialog ventana = new TextInputDialog();
                ventana.setTitle("CONDICION");
                ventana.setHeaderText(null);
                ventana.setContentText("Ingrese una condicion: ");
                Optional<String> tCondicion = ventana.showAndWait();
                Condicion con = new Condicion();
                if (tCondicion.isPresent()) {
                    String textoCond = tCondicion.get();
                    if (seguirDibujando) {
                        ejeX = mouse.getX();
                        ejeY = mouse.getY();
                        seguirDibujando = false;
                    } 
                    if (!seguirDibujando && verificarOrillas(ejeX, ejeY) && verificarFigura(ejeY, ejeX)) {
                        Color colorFigura = buscarColorFigura("condicion", con);
                        Group nuevaCon = con.Dibujar(ejeX, ejeY, textoCond, colorFigura);
                        con.setX1(ejeX);
                        con.setY1(ejeY);
                        con.setNom(textoCond);
                        //agregarFigura(con);
                        figuras.add(con); 
                        //diagrama.add(nuevo); F                       
                        mostrar.getChildren().add(nuevaCon);
                        seguirDibujando = true;
                    }     
                    mostrar.setPrefWidth(mostrar.getPrefWidth()+500);
                    mostrar.setPrefHeight(mostrar.getPrefHeight()+500);
                    scroll.setContent(mostrar);

                }
            }
        });
    }
    @FXML
    private void Ciclo(ActionEvent event) {
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        seguirDibujando = true;
        /*VENTANA EMERGENTE QUE NOS PERMITE INGRESAR UNA CONDICION*/
            
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouse) {
                TextInputDialog ventana = new TextInputDialog();
                ventana.setTitle("CICLO");
                ventana.setHeaderText(null);
                ventana.setContentText("Ingrese una condicion: ");
                Optional<String> tCondicion = ventana.showAndWait();
                Ciclo con = new Ciclo();
                if (tCondicion.isPresent()) {
                    String textoCond = tCondicion.get();
                    if (seguirDibujando) {
                        ejeX = mouse.getX();
                        ejeY = mouse.getY();
                        seguirDibujando = false;
                    }
                    if (!seguirDibujando && verificarOrillas(ejeX, ejeY) && verificarFigura(ejeY, ejeX)) {
                        Color colorFigura = buscarColorFigura("ciclo", con);
                        Group nuevaCon = con.Dibujar(ejeX, ejeY, textoCond, colorFigura);
                        con.setX1(ejeX);
                        con.setY1(ejeY);
                        con.setNom(textoCond);
                        //agregarFigura(con);
                        figuras.add(con); 
                        //diagrama.add(nuevo); F                       
                        mostrar.getChildren().add(nuevaCon);
                        seguirDibujando = true;
                    }
                    mostrar.setPrefWidth(mostrar.getPrefWidth()+500);
                    mostrar.setPrefHeight(mostrar.getPrefHeight()+500);
                    scroll.setContent(mostrar);
                }


            }
        });
    }
    private ArrayList<Integer> condiseliminar= new ArrayList();
    private ArrayList<Integer> cicloeliminar= new ArrayList();
    private void eliminarSiguiente(int ahora, int ultimo){
        condiseliminar.add(ahora);
        if (figuras.get(ahora).isUnidosiguiente()&&figuras.get(ahora).getSiguiente()!=ultimo) {
            if (figuras.get(figuras.get(ahora).getSiguiente()) instanceof Condicion) {
                    eliminarCondi(figuras.get(ahora).getSiguiente());
                }
            
                else{
                    eliminarSiguiente(figuras.get(ahora).getSiguiente(),ultimo);
                }
        }
    }
    private void eliminarCondi(int c){
        condiseliminar.add(c);
        int corte=-1;
        if (((Condicion)figuras.get(c)).isUnidoder()) {
            if (figuras.get(((Condicion)figuras.get(c)).getDer()) instanceof Condicion) {
                eliminarCondi(((Condicion)figuras.get(c)).getDer());
            }
            else if (figuras.get(((Condicion)figuras.get(c)).getDer()) instanceof Ciclo) {
                eliminarCiclo(((Condicion)figuras.get(c)).getDer());
            }
            else{
                corte=eliminarSiguiente(((Condicion)figuras.get(c)).getDer());
            }            
        }
        if (corte!=-1 && figuras.get(c).isUnidosiguiente()) {
                eliminarSiguiente(figuras.get(c).getSiguiente(),corte);
        }
    }    
    private void eliminarCiclo(int c){
        cicloeliminar.add(c);        
        if (((Ciclo)figuras.get(c)).isUnidoizq()) {
            if (figuras.get(((Ciclo)figuras.get(c)).getIzq()) instanceof Condicion) {
                eliminarCondi(((Ciclo)figuras.get(c)).getIzq(),c);
            }            
            else{
                eliminarSiguiente2(((Ciclo)figuras.get(c)).getIzq(),c);
            }            
        }
        
    }
    
    private int eliminarSiguiente(int c){
        int cortar =-1;
        condiseliminar.add(c);
        if (figuras.get(c).isUnidosiguiente()) {
            for (int i = 0; i < lineas.size(); i++) {
                if (lineas.get(i).getIndicePartidaLista()==c) {
                    if (lineas.get(i).isCierra()) {
                        cortar = lineas.get(i).getIndiceLlegadaLista();
                    }
                }
                        
            }
            if (cortar==-1) {
                if (figuras.get(figuras.get(c).getSiguiente()) instanceof Condicion) {
                    eliminarCondi(figuras.get(c).getSiguiente());
                }                            
                else{
                    cortar = eliminarSiguiente(figuras.get(c).getSiguiente());
                }
            }           
        }
        return cortar;
    }
    private void eliminarCondi(int c, int frenar){
        cicloeliminar.add(c);
        int corte=-1;
        
        if (((Condicion)figuras.get(c)).isUnidoder() &&((Condicion)figuras.get(c)).getDer()!= frenar) {
            if (figuras.get(((Condicion)figuras.get(c)).getDer()) instanceof Condicion) {
                eliminarCondi(((Condicion)figuras.get(c)).getDer(),frenar);
            }            
            else{
                corte=eliminarSiguiente2(((Condicion)figuras.get(c)).getDer(),frenar);
            }            
        }
        if (corte!=-1 && figuras.get(c).isUnidosiguiente() && figuras.get(c).getSiguiente()!=frenar) {
                eliminarSiguiente(figuras.get(c).getSiguiente(),corte,frenar);
        }
    }    

    
    private int eliminarSiguiente2(int c,int frenar){
        int cortar =-1;
        cicloeliminar.add(c);
        if (figuras.get(c).isUnidosiguiente()&& figuras.get(c).getSiguiente()!=frenar) {
            for (int i = 0; i < lineas.size(); i++) {
                if (lineas.get(i).getIndicePartidaLista()==c) {
                    if (lineas.get(i).isCierra()) {
                        cortar = lineas.get(i).getIndiceLlegadaLista();
                    }
                }
                        
            }
            if (cortar==-1) {
                if (figuras.get(figuras.get(c).getSiguiente()) instanceof Condicion) {
                    eliminarCondi(figuras.get(c).getSiguiente());
                }            
                
                else{
                    cortar = eliminarSiguiente(figuras.get(c).getSiguiente());
                }
            }           
        }
        return cortar;
    }
    private void eliminarSiguiente(int ahora, int ultimo, int frenar){
        condiseliminar.add(ahora);
        if (figuras.get(ahora).isUnidosiguiente()&&figuras.get(ahora).getSiguiente()!=ultimo&&figuras.get(ahora).getSiguiente()!=frenar) {
            if (figuras.get(figuras.get(ahora).getSiguiente()) instanceof Condicion) {
                    eliminarCondi(figuras.get(ahora).getSiguiente(),frenar);
                }
            
                else{
                    eliminarSiguiente(figuras.get(ahora).getSiguiente(),ultimo,frenar);
                }
        }
    }
    @FXML
    private void EliminarElemento(ActionEvent event) {
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        seguirDibujando = true;
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouse) {
                /*OBTENEMOS EL EJE X Y EL EJE Y DONDE SE HACE CLICK*/
                if (seguirDibujando) {
                    ejeX = mouse.getX();
                    ejeY = mouse.getY();
                    seguirDibujando = false;
                }               
                for (int i = 0; i < figuras.size(); i++) {
                    if (probarsiexiste(ejeX,figuras.get(i).getX1(),ejeY,figuras.get(i).getY1())) {                        
                        if (figuras.get(i) instanceof Condicion) {                                                  
                            eliminarCondi(i);
                            Collections.sort(condiseliminar);
                            for (int j = condiseliminar.size()-1; j >=0 ; j--) {
                                int a =condiseliminar.get(j);                                
                                eliminarLineas(a);
                                figuras.remove(a);
                                restarelimina(a);
                                
                            }
                        }else if (figuras.get(i) instanceof Ciclo) {
                            eliminarCiclo(i);
                            Collections.sort(cicloeliminar);
                            elimiarRepetidos(cicloeliminar);
                            for (int j = cicloeliminar.size()-1; j >=0 ; j--) {
                                int a =cicloeliminar.get(j);                                
                                eliminarLineas(a);
                                figuras.remove(a);
                                restarelimina(a);
                                
                            }
                        }
                        else{
                            if ("Final".equals(figuras.get(i).getNom())){
                                bloquearAlInicio(false);
                                finalCil=true;
                            }
                            if ("Inicio".equals(figuras.get(i).getNom())){
                                bloquearAlInicio(true);
                                inicioFin.setDisable(false);
                                inicioCil=true;
                            }
                            eliminarLineas(i);
                            figuras.remove(i);          
                            restarelimina(i);
                            
                        }       
                        condiseliminar.clear();
                        mostrarwas();
                        //eliminarLineas(i);
                        //restarelimina(i);
                        //mostrarwas();
                    }
                }
                
            }
        });
    }
    private void elimiarRepetidos(ArrayList<Integer> ciclos){
       ArrayList<Integer> aux= new ArrayList();
       for (int i=0;i<ciclos.size();i++) {            
            if (!aux.contains(ciclos.get(i))) {  
                aux.add(ciclos.get(i)); 
            } 
       }
       cicloeliminar.clear();
       for (int i = 0; i < aux.size(); i++) {
           cicloeliminar.add(aux.get(i));
       }
   }
    @FXML
    private void Eliminar(ActionEvent event) {
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        mostrar.getChildren().clear();
        figuras.clear();
        lineas.clear();
        diagrama.clear();
        datosConsola.clear();
        root.getChildren().clear();
        consola.getChildren().clear();
        
        System.out.println(consola.getChildren().size());
        inicioCil=true;
        finalCil=true;
        bloquearAlInicio(true);
        inicioFin.setDisable(false);
        mostrar.setPrefHeight(valorxCanvas);
        mostrar.setPrefWidth(valoryCanvas);
        scroll.setContent(mostrar);
    }
    private int buscarlineaconectada(int c){
        int a =-1;
        for (int i = 0; i < lineas.size(); i++) {
            if (lineas.get(i).getIndicePartidaLista()==c) {
                a=i;
                break;
            }
        }
        return a;
    }
    private void añadirespaciosCiclo(ArrayList<String> arreglo, int partida){
        int lugar=-1;
        for (int i = 0; i < arreglo.size(); i++) {
            if (figuras.get(partida).getNom().equals(arreglo.get(i))) {
                lugar=i;
            }
        }
        arreglo.add(lugar,"do");
        for (int i = lugar+1; i < arreglo.size()-1; i++) {
            arreglo.set(i, "    "+arreglo.get(i));
        }
    }
    private void añadirCiclo(ArrayList<String> arreglo, int partida){
        arreglo.add("while : "+figuras.get(partida).getNom());
        añadirespaciosCiclo(arreglo,((Ciclo)figuras.get(partida)).getIzq());
    }
    private int añadirCondi(ArrayList<String> arreglo, int partida){
        arreglo.add("if : "+figuras.get(partida).getNom());
        int empiezaif=arreglo.size();
        int corte=-1;
        if (figuras.get(((Condicion)figuras.get(partida)).getDer()) instanceof Condicion) {
            añadirCondi(arreglo, ((Condicion)figuras.get(partida)).getDer());
        }
        else{
            corte = añadirVerda(arreglo,((Condicion)figuras.get(partida)).getDer());
        }
        añadirespacioCondi(arreglo,empiezaif);
        if (figuras.get(partida).getSiguiente()!=corte) {
            arreglo.add("else : ");
            empiezaif=arreglo.size();
            añadirVerda(arreglo,figuras.get(partida).getSiguiente());
            añadirespacioCondi(arreglo,empiezaif);
        }
        return corte;
    }
    private void añadirespacioCondi(ArrayList<String> arreglo, int partida){
        for (int i = partida; i < arreglo.size(); i++) {
            arreglo.set(i, "    "+arreglo.get(i));
        }
    }
    private int añadirVerda(ArrayList<String> arreglo, int partida){
        arreglo.add(figuras.get(partida).getNom());
        int linea= buscarlineaconectada(partida);
        int a=-1;
        if (lineas.get(linea).isCierra()) {
            a=figuras.get(partida).getSiguiente();
        }
        else{
            if (figuras.get(figuras.get(partida).getSiguiente()) instanceof Condicion) {
                añadirCondi(arreglo,figuras.get(partida).getSiguiente());
            }
            else if (figuras.get(figuras.get(partida).getSiguiente()) instanceof Ciclo) {
                añadirCiclo(arreglo,figuras.get(partida).getSiguiente());
            }
            else{
                a=añadirVerda(arreglo,figuras.get(partida).getSiguiente());
            }
            
        }
        return a;
    }
    private ArrayList<String> obtenerString(){
        int posicion=figuras.get(0).getSiguiente();        
        ArrayList<String> contenido = new ArrayList();
        contenido.add("Inico");
        while(posicion!=figuras.size()-1){
                      
            if (figuras.get(posicion) instanceof Ciclo) {
                añadirCiclo(contenido,posicion);
                posicion= figuras.get(posicion).getSiguiente();
            }
            else if (figuras.get(posicion) instanceof Condicion) {
                posicion= añadirCondi(contenido,posicion);
            }
            else{
                contenido.add(figuras.get(posicion).getNom());
                posicion= figuras.get(posicion).getSiguiente();
            }
            
            
        }
        contenido.add("Fin");
        for (int i = 1; i < contenido.size()-1; i++) {
            contenido.set(i, "    "+contenido.get(i));
        }
        
        posicion =0;
        return contenido;
    }
    Group sennal = new Group();
    int valor = 0;
    boolean termino=false;;
    @FXML
    private void Ejecutar(ActionEvent event) {
        if (comprobardiagrama()) {
            ArrayList<Text> mostrarConsola2 = new ArrayList<>();
            /*SI EL VALOR YA AVANZO AL MENOS UNO LIMPIAMOS EL GRUPO DE SEÑALIZADORES Y LO SACAMOS DEL PANE
        PARA QUE ASI SE BORRE LA SEÑAL ANTERIOR*/
            if (valor > 0) {
                sennal.getChildren().clear();
                mostrar.getChildren().remove(sennal);
            }
            if (termino) {
                valor = 0;
                //ejecutarDiagrama.clear();
                mostrar.getChildren().clear();
                datosConsola.clear();
                consola.getChildren().clear();
                mostrarwas();
                
                if (verificarFinal()) {
                    bloquearAlFinal();
                }
                termino = false;
            }else {
                /*CREAMOS SEÑALIZADOR*/
                double xUno = figuras.get(valor).getX1() - 30;
                double yUno = figuras.get(valor).getY1() + 25;
                Line flecha = new Line(xUno - 100, yUno, xUno - 70, yUno);
                Line flecha1 = new Line(xUno - 70, yUno - 10, xUno - 70, yUno + 10);
                Line flecha2 = new Line(xUno - 70, yUno - 10, xUno - 50, yUno);
                Line flecha3 = new Line(xUno - 70, yUno + 10, xUno - 50, yUno);

                flecha.setStrokeWidth(5);
                flecha.setStroke(Color.RED);
                flecha1.setStrokeWidth(5);
                flecha1.setStroke(Color.RED);
                flecha2.setStrokeWidth(5);
                flecha2.setStroke(Color.RED);
                flecha3.setStrokeWidth(5);
                flecha3.setStroke(Color.RED);
                sennal.getChildren().addAll(flecha, flecha1, flecha2, flecha3);
                mostrar.getChildren().add(sennal);

                if (valor == 0) {
                    consola.getChildren().clear();
                    mostrarConsola2.clear();
                }
                if (termino) {
                    consola.getChildren().clear();
                    //mostrarConsola();

                } else {

                    if (figuras.get(valor) instanceof Entrasal) {
                        String TextoGroup = figuras.get(valor).getNom();
                        if (Funciones.VerEntradaSalida(TextoGroup)) {
                            String[] parts;
                            parts = TextoGroup.split(",");
                            parts[1] = parts[1].replace(" ", "");
                            parts[0] = parts[0].replace("\"", "");
                            for (int i = 0; i < datosConsola.size(); i++) {
                                if (parts[1] == null ? datosConsola.get(i).getNom() == null : parts[1].equals(datosConsola.get(i).getNom())) {
                                    parts[1] = datosConsola.get(i).getValor();
                                }
                            }
                            textoAux = parts[0] + parts[1]; //Cambiamos el = por el <-
                            Text codigo = new Text(textoAux + "\n"); //Pasamos el string que recibimos a texto
                            codigo.setFill(Color.BLACK);
                            codigo.setFont(letra);
                            mostrarConsola2.add(codigo);
                        } else {
                            if (TextoGroup.contains("=")) {
                                String[] parts;
                                parts = TextoGroup.split("=");
                                GuardarVariable nuevaVari = new GuardarVariable(parts[0], parts[1]);
                                datosConsola.add(nuevaVari);
                            }
                            textoAux = TextoGroup.replace("=", "<-"); //Cambiamos el = por el <-
                            Text codigo = new Text(textoAux + "\n"); //Pasamos el string que recibimos a texto
                            codigo.setFill(Color.BLACK);
                            codigo.setFont(letra);
                            mostrarConsola2.add(codigo);
                        }

                    }
                    if (figuras.get(valor) instanceof Docu) {
                        String stringAux = figuras.get(valor).getNom();
                        Text codigo = new Text(stringAux + "\n");
                        codigo.setFill(Color.BLACK);
                        codigo.setFont(new Font("Lucida Console", 15));
                        mostrarConsola2.add(codigo);
                    }
                    if (figuras.get(valor) instanceof Proceso) {
                        String TextoGroup = figuras.get(valor).getNom();
                        String[] parts;
                        parts = TextoGroup.split("=");
                        Funciones.parseoProceso(parts[0], parts[1], datosConsola);
                        for (int i = 0; i < datosConsola.size(); i++) {
                            if (parts[0] == null ? datosConsola.get(i).getNom() == null : parts[0].equals(datosConsola.get(i).getNom())) {
                                String stringAux = datosConsola.get(i).getNom() + " <- " + datosConsola.get(i).getValor();
                                Text codigo = new Text(stringAux + "\n");
                                codigo.setFill(Color.BLACK);
                                codigo.setFont(new Font("Lucida Console", 15));
                                mostrarConsola2.add(codigo);

                            }
                        }

                    }
                    TextFlow root2 = new TextFlow();
                    consola.getChildren().clear();
                    root2.getChildren().addAll(mostrarConsola2);
                    consola.getChildren().addAll(root2);
                    if (figuras.get(valor) instanceof Condicion) {
                        String auxiliarBoolean = Funciones.reemplazarVariablesCondiciones(figuras.get(valor).getNom(), datosConsola);
                        boolean prueba = Funciones.condicionGeneral(auxiliarBoolean);
                        if (prueba) {
                            if (((Condicion) figuras.get(valor)).isUnidoder()) {
                                valor = ((Condicion) figuras.get(valor)).getDer();
                            } else {
                                termino = true;
                            }
                        } else {
                            if (figuras.get(valor).isUnidosiguiente()) {
                                valor = figuras.get(valor).getSiguiente();
                            } else {
                                termino = true;
                            }

                        }
                    } else if (figuras.get(valor) instanceof Ciclo) {
                        String auxiliarBoolean = Funciones.reemplazarVariablesCondiciones(figuras.get(valor).getNom(), datosConsola);
                        boolean prueba = Funciones.condicionGeneral(auxiliarBoolean);

                        if (prueba) {
                            if (((Ciclo) figuras.get(valor)).isUnidoizq()) {
                                valor = ((Ciclo) figuras.get(valor)).getIzq();
                            } else {
                                termino = true;
                            }
                        } else {
                            if (figuras.get(valor).isUnidosiguiente()) {
                                valor = figuras.get(valor).getSiguiente();
                            } else {
                                termino = true;
                            }
                        }
                    } else {
                        if (figuras.get(valor).isUnidosiguiente()) {
                            valor = figuras.get(valor).getSiguiente();
                        } else {
                            termino = true;
                        }
                    }

                }
            }
        }

    }
    
    @FXML
    private void Ejecutar2(ActionEvent event) throws InterruptedException {
        if (comprobardiagrama()) {
            valor = 0;
            termino = false;
            datosConsola.clear();

            ArrayList<Text> mostrarConsola2 = new ArrayList<>();
            mostrarConsola2.clear();
            while (!termino) {

                Group auxGroupCorrer = figuras.get(valor).getNuevo();
                Label numeroGroup = (Label) auxGroupCorrer.getChildren().get(1);
                int numeroGr = Integer.parseInt(numeroGroup.getText());

                if (numeroGr == 1) {
                    Label TextoGroup = (Label) auxGroupCorrer.getChildren().get(auxGroupCorrer.getChildren().size() - 2);
                    if (Funciones.VerEntradaSalida(TextoGroup.getText())) {
                        String[] parts;
                        parts = TextoGroup.getText().split(",");
                        parts[1] = parts[1].replace(" ", "");
                        parts[0] = parts[0].replace("\"", "");
                        for (int i = 0; i < datosConsola.size(); i++) {
                            if (parts[1] == null ? datosConsola.get(i).getNom() == null : parts[1].equals(datosConsola.get(i).getNom())) {
                                parts[1] = datosConsola.get(i).getValor();
                            }
                        }
                        textoAux = parts[0] + parts[1]; //Cambiamos el = por el <-
                        Text codigo = new Text(textoAux + "\n"); //Pasamos el string que recibimos a texto
                        codigo.setFill(Color.BLACK);
                        codigo.setFont(letra);
                        mostrarConsola2.add(codigo);
                    } else {
                        if (TextoGroup.getText().contains("=")) {
                            String[] parts;
                            parts = TextoGroup.getText().split("=");
                            GuardarVariable nuevaVari = new GuardarVariable(parts[0], parts[1]);
                            datosConsola.add(nuevaVari);
                        }
                        textoAux = TextoGroup.getText().replace("=", "<-"); //Cambiamos el = por el <-
                        Text codigo = new Text(textoAux + "\n"); //Pasamos el string que recibimos a texto
                        codigo.setFill(Color.BLACK);
                        codigo.setFont(letra);
                        mostrarConsola2.add(codigo);

                    }

                }
                if (numeroGr == 3) {
                    Label TextoGroup = (Label) auxGroupCorrer.getChildren().get(auxGroupCorrer.getChildren().size() - 2);
                    String stringAux = TextoGroup.getText();

                    Text codigo = new Text(stringAux + "\n");
                    codigo.setFill(Color.BLACK);
                    codigo.setFont(new Font("Lucida Console", 15));
                    mostrarConsola2.add(codigo);
                }
                if (numeroGr == 4) {
                    Label TextoGroup = (Label) auxGroupCorrer.getChildren().get(auxGroupCorrer.getChildren().size() - 2);
                    String[] parts;
                    parts = TextoGroup.getText().split("=");
                    Funciones.parseoProceso(parts[0], parts[1], datosConsola);
                    for (int i = 0; i < datosConsola.size(); i++) {
                        if (parts[0] == null ? datosConsola.get(i).getNom() == null : parts[0].equals(datosConsola.get(i).getNom())) {
                            String stringAux = datosConsola.get(i).getNom() + " <- " + datosConsola.get(i).getValor();
                            Text codigo = new Text(stringAux + "\n");
                            codigo.setFill(Color.BLACK);
                            codigo.setFont(new Font("Lucida Console", 15));
                            mostrarConsola2.add(codigo);

                        }
                    }

                }
                if (numeroGr == 5 || numeroGr == 6) {
                    Label TextoGroup = (Label) auxGroupCorrer.getChildren().get(auxGroupCorrer.getChildren().size() - 2);
                    String stringAux = TextoGroup.getText();

                    Text codigo = new Text(stringAux + "\n");
                    codigo.setFill(Color.BLACK);
                    codigo.setFont(new Font("Lucida Console", 15));
                    mostrarConsola2.add(codigo);
                }

                if (figuras.get(valor) instanceof Condicion) {
                    String auxiliarBoolean = Funciones.reemplazarVariablesCondiciones(figuras.get(valor).getNom(), datosConsola);
                    boolean prueba = Funciones.condicionGeneral(auxiliarBoolean);
                    if (prueba) {
                        if (((Condicion) figuras.get(valor)).isUnidoder()) {
                            valor = ((Condicion) figuras.get(valor)).getDer();
                        } else {
                            termino = true;
                        }
                    } else {
                        if (figuras.get(valor).isUnidosiguiente()) {
                            valor = figuras.get(valor).getSiguiente();
                        } else {
                            termino = true;
                        }

                    }
                } else if (figuras.get(valor) instanceof Ciclo) {
                    String auxiliarBoolean = Funciones.reemplazarVariablesCondiciones(figuras.get(valor).getNom(), datosConsola);
                    boolean prueba = Funciones.condicionGeneral(auxiliarBoolean);

                    if (prueba) {
                        if (((Ciclo) figuras.get(valor)).isUnidoizq()) {
                            valor = ((Ciclo) figuras.get(valor)).getIzq();
                        } else {
                            termino = true;
                        }
                    } else {
                        if (figuras.get(valor).isUnidosiguiente()) {
                            valor = figuras.get(valor).getSiguiente();
                        } else {
                            termino = true;
                        }
                    }
                } else {
                    if (figuras.get(valor).isUnidosiguiente()) {
                        valor = figuras.get(valor).getSiguiente();
                    } else {
                        termino = true;
                    }
                }
                TextFlow root2 = new TextFlow();
                consola.getChildren().clear();
                root2.getChildren().addAll(mostrarConsola2);
                consola.getChildren().addAll(root2);
            }
        }

    }

    @FXML
    private void Mover(ActionEvent event) {
        seguirDibujando = true;
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (seguirDibujando) {
                    ejeX=event.getX();
                    ejeY=event.getY();
                    seguirDibujando = false;
                }
                for (int i = 0; i < figuras.size(); i++) {
                    if (ejeX>=figuras.get(i).getX1()-50 && ejeX<=figuras.get(i).getX1()+50 && ejeY>=figuras.get(i).getY1() && ejeY<=figuras.get(i).getY1()+100 ) {
                        if (figuras.get(i) instanceof Infin) {
                            
                        }
                        else{
                            moverFigura(event, i, figuras.get(i).getNom(), figuras.get(i));                            
                        }
                    }
                }
            }
            
        });
    }
    double ejeXAuxiliar, ejeYAuxiliar;
    boolean moverIndv;
    private void moverFigura(MouseEvent event, int num, String nombre, Figuras figuraIngre){
        moverIndv=true;
        mostrar.setOnMousePressed(e ->{
            ejeXAuxiliar = e.getX();
            ejeYAuxiliar = e.getY();
        });
        mostrar.setOnMouseDragged(e ->{
            if (moverIndv &&  ejeXAuxiliar>=figuraIngre.getX1()-50 && ejeXAuxiliar<=figuraIngre.getX1()+50 && 
                    ejeYAuxiliar>=figuraIngre.getY1() && ejeYAuxiliar<=figuraIngre.getY1()+100){
                if (figuraIngre instanceof Entrasal) {
                    boolean anterior=figuras.get(num).isUnidoanterior();
                    boolean siguiente=figuras.get(num).isUnidosiguiente();
                    int sig=figuras.get(num).getSiguiente();
                    figuras.remove(num);
                    Entrasal entraSal = new Entrasal();
                    entraSal.setNom(nombre);
                    entraSal.setX1(e.getX());
                    entraSal.setY1(e.getY());
                    entraSal.setUnidoanterior(anterior);
                    if (siguiente) {
                        entraSal.setUnidosiguiente(siguiente);
                        entraSal.setSiguiente(sig);
                    }
                    entraSal.setNuevo(new Group(entraSal.Dibujar(e.getX(), e.getY(), nombre, figuraIngre.getColorFig())));
                    
                    figuras.add(num, entraSal);
                }
                if (figuraIngre instanceof Proceso) {
                    boolean anterior=figuras.get(num).isUnidoanterior();
                    boolean siguiente=figuras.get(num).isUnidosiguiente();
                    int sig=figuras.get(num).getSiguiente();
                    figuras.remove(num);
                    Proceso procesoProc = new Proceso();
                    procesoProc.setNom(nombre);
                    procesoProc.setX1(e.getX());
                    procesoProc.setY1(e.getY());
                    procesoProc.setUnidoanterior(anterior);
                    if (siguiente) {
                        procesoProc.setUnidosiguiente(siguiente);
                        procesoProc.setSiguiente(sig);
                    }
                    procesoProc.setNuevo(new Group(procesoProc.Dibujar(e.getX(), e.getY(), nombre, figuraIngre.getColorFig())));
                    figuras.add(num, procesoProc);
                }
                if (figuraIngre instanceof Condicion) {
                    boolean anterior=figuras.get(num).isUnidoanterior();
                    boolean siguiente=figuras.get(num).isUnidosiguiente();
                    int sig=figuras.get(num).getSiguiente();
                    boolean derecha=((Condicion)figuras.get(num)).isUnidoder();
                    int der=((Condicion)figuras.get(num)).getDer();
                    figuras.remove(num);
                    Condicion condiCond = new Condicion();
                    condiCond.setNom(nombre);
                    condiCond.setX1(e.getX());
                    condiCond.setY1(e.getY());
                    condiCond.setUnidoanterior(anterior);
                    if (siguiente) {
                        condiCond.setUnidosiguiente(siguiente);
                        condiCond.setSiguiente(sig);
                    }
                    if (derecha) {
                        condiCond.setUnidoder(derecha);
                        condiCond.setDer(der);
                    }
                    condiCond.setNuevo(new Group(condiCond.Dibujar(e.getX(), e.getY(), nombre, figuraIngre.getColorFig())));
                    figuras.add(num, condiCond);
                }
                if (figuraIngre instanceof Docu) {
                    boolean anterior=figuras.get(num).isUnidoanterior();
                    boolean siguiente=figuras.get(num).isUnidosiguiente();
                    int sig=figuras.get(num).getSiguiente();
                    figuras.remove(num);
                    Docu documentoDocu = new Docu();
                    documentoDocu.setNom(nombre);
                    documentoDocu.setX1(e.getX());
                    documentoDocu.setY1(e.getY());
                    documentoDocu.setUnidoanterior(anterior);
                    if (siguiente) {
                        documentoDocu.setUnidosiguiente(siguiente);
                        documentoDocu.setSiguiente(sig);
                    }
                    documentoDocu.setNuevo(new Group(documentoDocu.Dibujar(e.getX(), e.getY(), nombre, figuraIngre.getColorFig())));
                    figuras.add(num, documentoDocu);
                }
                if (figuraIngre instanceof Ciclo) {
                    boolean anterior=figuras.get(num).isUnidoanterior();
                    boolean siguiente=figuras.get(num).isUnidosiguiente();
                    int sig=figuras.get(num).getSiguiente();
                    boolean izquierda=((Ciclo)figuras.get(num)).isUnidoizq();
                    int izq=((Ciclo)figuras.get(num)).getIzq();
                    figuras.remove(num);
                    Ciclo condiCond = new Ciclo();
                    condiCond.setNom(nombre);
                    condiCond.setX1(e.getX());
                    condiCond.setY1(e.getY());
                    condiCond.setUnidoanterior(anterior);
                    if (siguiente) {
                        condiCond.setUnidosiguiente(siguiente);
                        condiCond.setSiguiente(sig);
                    }
                    if (izquierda) {
                        condiCond.setUnidoizq(izquierda);
                        condiCond.setIzq(izq);
                    }
                    condiCond.setNuevo(new Group(condiCond.Dibujar(e.getX(), e.getY(), nombre, figuraIngre.getColorFig())));
                    figuras.add(num, condiCond);
                }
                moverlineas(num);
                mostrarwas();
            }
            
        });
    }
    private void moverlineas(int pos){
        int abc=-1;
        for (int i = 0; i < lineas.size(); i++) {
            if (lineas.get(i).getIndicePartidaLista()==pos) {
                abc=lineas.get(i).getIndiceLlegadaLista();
                break;
            }
        }
        for (int i = 0; i < lineas.size(); i++) {
            if (lineas.get(i).getIndiceLlegadaLista() == pos ||lineas.get(i).getIndicePartidaLista() == pos||lineas.get(i).getIndiceLlegadaLista() == abc ||lineas.get(i).getIndicePartidaLista() == abc) {
                if (lineas.get(i).isCierra()) {
                    int partida =lineas.get(i).getIndicePartidaLista();
                    int llegada =lineas.get(i).getIndiceLlegadaLista();                    
                    lineas.remove(i);
                    Flujo flujo = new Flujo();
                    flujo.setIndicePartidaLista(partida);
                    flujo.setIndiceLlegadaLista(llegada);
                    flujo.setCierra(true);
                    if (figuras.get(partida) instanceof Condicion || figuras.get(partida) instanceof Ciclo) {                        
                        
                        double x2=0, y2=0;
                        for (int j = 0; j < lineas.size(); j++) {
                            if (lineas.get(j).getIndiceLlegadaLista()==llegada) {
                                x2=(lineas.get(j).getX1()+lineas.get(j).getX2())/2;
                                
                                y2=(lineas.get(j).getY1()+lineas.get(j).getY2())/2;
                                break;
                                
                            }
                        }
                        flujo.Dibujar2(figuras.get(partida).getX1(), figuras.get(partida).getY1()+100, x2, y2);
             
                    }
                    else{
                        double x2=0, y2=0;
                        for (int j = 0; j < lineas.size(); j++) {
                            if (lineas.get(j).getIndiceLlegadaLista()==llegada) {
                                x2=(lineas.get(j).getX1()+lineas.get(j).getX2())/2;
                                
                                y2=(lineas.get(j).getY1()+lineas.get(j).getY2())/2;
                                break;
                            }
                        }
                        flujo.Dibujar2(figuras.get(partida).getX1(), figuras.get(partida).getY1()+50, x2, y2);
                    }
                    lineas.add(i, flujo);
                }
                else if (lineas.get(i).isCiclo()) {
                    int partida =lineas.get(i).getIndicePartidaLista();
                    int llegada = lineas.get(i).getIndiceLlegadaLista();
                    lineas.remove(i);
                    Flujo flujo = new Flujo();
                    flujo.setIndicePartidaLista(partida);
                    flujo.setIndiceLlegadaLista(llegada);
                    flujo.setCiclo(true);
                    double x2 = 0, y2 = 0;
                    for (int j = 0; j < lineas.size(); j++) {
                        if (lineas.get(j).getIndiceLlegadaLista() == llegada) {
                            x2 = (lineas.get(j).getX1() + lineas.get(j).getX2()) / 2;

                            y2 = (lineas.get(j).getY1() + lineas.get(j).getY2()) / 2;
                            break;
                        }
                    }
                    flujo.Dibujar3(figuras.get(partida).getX1(), figuras.get(partida).getY1() + 50, x2, y2);
                    lineas.add(i, flujo);
                    
                }
                else{
                    int partida =lineas.get(i).getIndicePartidaLista();
                    int llegada =lineas.get(i).getIndiceLlegadaLista();                    
                    lineas.remove(i);
                    Flujo flujo = new Flujo();
                    flujo.setIndicePartidaLista(partida);
                    flujo.setIndiceLlegadaLista(llegada);
                    if (figuras.get(partida) instanceof Condicion || figuras.get(partida) instanceof Ciclo) {
                        if (figuras.get(partida) instanceof Condicion && llegada ==((Condicion)figuras.get(partida)).getDer()) {
                            flujo.Dibujar(figuras.get(partida).getX1()+50, figuras.get(partida).getY1()+50, figuras.get(llegada).getX1(), figuras.get(llegada).getY1());
                        }
                        else{
                            flujo.Dibujar(figuras.get(partida).getX1(), figuras.get(partida).getY1()+100, figuras.get(llegada).getX1(), figuras.get(llegada).getY1());
                        }
                    }
                    else{
                        flujo.Dibujar(figuras.get(partida).getX1(), figuras.get(partida).getY1()+50, figuras.get(llegada).getX1(), figuras.get(llegada).getY1());
                    }
                    lineas.add(i, flujo);
                }                
            }
            
        }
    }
    
    @FXML
    private void Exportar(ActionEvent event) throws IOException{
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        Stage theStage = FlowChart.getPrimaryStage();
        List<String> choices = new ArrayList<>();
        choices.add("PNG");
        choices.add("JPG");
        choices.add("PDF");
        choices.add("PSEUDOCODIGO");

        ChoiceDialog<String> dialog = new ChoiceDialog<>("PNG", choices);
        dialog.setTitle("Exportar Diagrama");
        dialog.setHeaderText("Seleccione la opcion de exportacion:");
        dialog.setContentText(null);
        
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            if ("PNG".equals(result.get())) {
                Exportar.exportarPNG(theStage, Funciones.valorMaximoXArreglo(figuras)+200, Funciones.valorMaximoYArreglo(figuras)+200, mostrar);
            }
            if ("JPG".equals(result.get())) {
                Exportar.exportarJPG(theStage, Funciones.valorMaximoXArreglo(figuras)+200, Funciones.valorMaximoYArreglo(figuras)+200, mostrar);
            }
            if ("PDF".equals(result.get())) {
                Exportar.exportarAPDF(theStage, Funciones.valorMaximoXArreglo(figuras)+200, Funciones.valorMaximoYArreglo(figuras)+200, mostrar);
            }
            if ("PSEUDOCODIGO".equals(result.get()) &&comprobardiagrama()) {
                Exportar.exportaratxt(obtenerString());
            }
            
        }
    }
    
    @FXML
    private void cambiarNombre(ActionEvent event) throws IOException{
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount()==2) {
                    Figuras nueva = Funciones.devolverFigura(figuras, event.getX(), event.getY());
                    if (nueva==null) {
                    }
                    if (nueva instanceof Infin) {
                        Alert alert = new Alert(AlertType.ERROR);
                        alert.setTitle("ERROR");
                        alert.setHeaderText(null);
                        alert.setContentText("No se puede cambiar el nombre.");

                        alert.showAndWait();
                    }
                    else{
                        TextInputDialog ventana = new TextInputDialog(nueva.getNom());
                        ventana.setTitle("CAMBIO DE NOMBRE");
                        ventana.setHeaderText(null);
                        ventana.setContentText("Ingrese Nombre: ");
                        Optional<String> nuevoNombre = ventana.showAndWait();
                        if (nuevoNombre.isPresent()) {
                            boolean verificacionCambioTexto=true;
                            String nuevoNombreStr = nuevoNombre.get();
                            if (nueva instanceof Entrasal) {
                                if (!Funciones.VerificacionTextoEntrada(nuevoNombreStr)) {
                                    verificacionCambioTexto=false;
                                }
                            }
                            if (nueva instanceof Proceso) {
                                if (!Funciones.VerificacionTextoProceso(nuevoNombreStr)) {
                                    verificacionCambioTexto=false;
                                }
                            }
                            
                            if (verificacionCambioTexto) {
                                int valorFigura = Funciones.buscarIndice(figuras, nueva);
                                if (valorFigura>=0) {
                                    nueva.setNom(nuevoNombreStr);
                                    Group nuevoGroup = nueva.Dibujar(nueva.getX1(), nueva.getY1(), nuevoNombreStr, colorpicker.getValue());
                                    nueva.setNuevo(nuevoGroup);
                                    figuras.set(valorFigura, nueva);
                                    mostrarwas();
                                }
                            }
                            if (!verificacionCambioTexto) {
                                Alert alert = new Alert(AlertType.ERROR);
                                alert.setTitle("INFORMACION");
                                alert.setHeaderText(null);
                                alert.setContentText("Los datos ingresados no son los adecuados.");

                                alert.showAndWait();
                            }
                            
                        }
                        
                    }
                }
                
            }
            
        });
    }

    @FXML
    private void cortar(ActionEvent event) {
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        seguirDibujando = true;
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouse) {
                /*OBTENEMOS EL EJE X Y EL EJE Y DONDE SE HACE CLICK*/
                if (seguirDibujando) {
                    ejeX = mouse.getX();
                    ejeY = mouse.getY();
                    seguirDibujando = false;
                }
                for (int i = 0; i < figuras.size(); i++) {
                    if (probarsiexiste(ejeX,figuras.get(i).getX1(),ejeY,figuras.get(i).getY1())) {
                        unir2(event,ejeX,ejeY);
                    }
                }
            }

        });
    }
    private void unir2(ActionEvent event, double x1, double y1){
        seguirDibujando = true;
        figura1=-1;        
        figura2=-1;
        for (int i = 0; i < figuras.size(); i++) {
                    if (probarsiexiste(x1,figuras.get(i).getX1(),y1,figuras.get(i).getY1())) {
                        figura1=i;                                               
                    }
                }
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            
            @Override
            public void handle(MouseEvent mouse) {
                /*OBTENEMOS EL EJE X Y EL EJE Y DONDE SE HACE CLICK*/
                if (seguirDibujando) {
                    ejeX = mouse.getX();
                    ejeY = mouse.getY();
                    seguirDibujando = false;
                }
                for (int i = 0; i < figuras.size(); i++) {
                    
                    if (probarsiexiste(ejeX,figuras.get(i).getX1(),ejeY,figuras.get(i).getY1())) {
                        figura2=i;               
                        
                    }
                }
                if (!figuras.get(figura1).isUnidosiguiente() && figuras.get(figura2).isUnidoanterior() && figura1!=figura2) {
                    if (figuras.get(figura1) instanceof Condicion) {
                        Flujo f = new Flujo();
                        double x2=0, y2=0;
                        for (int i = 0; i < lineas.size(); i++) {
                            if (lineas.get(i).getIndiceLlegadaLista()==figura2) {
                                x2=(lineas.get(i).getX1()+lineas.get(i).getX2())/2;
                                
                                y2=(lineas.get(i).getY1()+lineas.get(i).getY2())/2;
                                break;
                            }
                        }
                        Group Dibujar = f.Dibujar2(figuras.get(figura1).getX1(), figuras.get(figura1).getY1()+100, x2, y2);
                        f.setIndicePartidaLista(figura1);
                        f.setIndiceLlegadaLista(figura2);
                        lineas.add(f);
                        mostrar.getChildren().add(Dibujar);                        
                        
                    }
                    else{
                        Flujo f = new Flujo();
                        double x2=0, y2=0;
                        for (int i = 0; i < lineas.size(); i++) {
                            if (lineas.get(i).getIndiceLlegadaLista()==figura2) {
                                x2=(lineas.get(i).getX1()+lineas.get(i).getX2())/2;
                                
                                y2=(lineas.get(i).getY1()+lineas.get(i).getY2())/2;
                                break;
                                
                            }
                        }
                        Group Dibujar = f.Dibujar2(figuras.get(figura1).getX1(), figuras.get(figura1).getY1()+50, x2, y2);
                        f.setIndicePartidaLista(figura1);
                        f.setIndiceLlegadaLista(figura2);
                        lineas.add(f);
                        mostrar.getChildren().add(Dibujar);
                    }
                    figuras.get(figura1).setUnidosiguiente(true);                    
                    figuras.get(figura1).setSiguiente(figura2);
                    
                }                
            }

        });
    }
    @FXML
    private void juntarciclo(ActionEvent event) {
        sennal.getChildren().clear();
        mostrar.getChildren().remove(sennal);
        seguirDibujando = true;
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouse) {
                /*OBTENEMOS EL EJE X Y EL EJE Y DONDE SE HACE CLICK*/
                if (seguirDibujando) {
                    ejeX = mouse.getX();
                    ejeY = mouse.getY();
                    seguirDibujando = false;
                }
                for (int i = 0; i < figuras.size(); i++) {
                    if (probarsiexiste(ejeX,figuras.get(i).getX1(),ejeY,figuras.get(i).getY1())) {
                        if (figuras.get(i) instanceof Ciclo) {
                            unir3(event,ejeX,ejeY);
                        }                       
                        
                    }
                }
            }

        });
    }
    private void unir3(ActionEvent event, double x1, double y1){
        seguirDibujando = true;
        figura1=-1;        
        figura2=-1;
        for (int i = 0; i < figuras.size(); i++) {
                    if (probarsiexiste(x1,figuras.get(i).getX1(),y1,figuras.get(i).getY1())) {
                        figura1=i;                                               
                    }
                }
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            
            @Override
            public void handle(MouseEvent mouse) {
                /*OBTENEMOS EL EJE X Y EL EJE Y DONDE SE HACE CLICK*/
                if (seguirDibujando) {
                    ejeX = mouse.getX();
                    ejeY = mouse.getY();
                    seguirDibujando = false;
                }
                for (int i = 0; i < figuras.size(); i++) {
                    
                    if (probarsiexiste(ejeX,figuras.get(i).getX1(),ejeY,figuras.get(i).getY1())) {
                        figura2=i;               
                        
                    }
                }
                if (figuras.get(figura2).isUnidoanterior() && !(figuras.get(figura2) instanceof Ciclo) &&(figuras.get(figura1).getY1()>figuras.get(figura2).getY1()) && figura1!=figura2) {
                    
                        Flujo f = new Flujo();
                        double x2=0, y2=0;
                        for (int i = 0; i < lineas.size(); i++) {
                            if (lineas.get(i).getIndiceLlegadaLista()==figura2) {
                                x2=(lineas.get(i).getX1()+lineas.get(i).getX2())/2;
                                
                                y2=(lineas.get(i).getY1()+lineas.get(i).getY2())/2;
                                break;  
                            }
                        }
                        Group Dibujar = f.Dibujar3(figuras.get(figura1).getX1(), figuras.get(figura1).getY1()+50, x2, y2);
                        f.setIndicePartidaLista(figura1);
                        f.setIndiceLlegadaLista(figura2);
                        lineas.add(f);
                        mostrar.getChildren().add(Dibujar);
                    
                    ((Ciclo)figuras.get(figura1)).setUnidoizq(true); 
                    ((Ciclo)figuras.get(figura1)).setIzq(figura2);
                    
                }                
            }

        });
        
    }
    

    
    @FXML
    private void pintarFigura(ActionEvent event) {
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 1) {
                    /*BUSCAMOS LA FIGURA SELECCIONADA*/
                    Figuras figSeleccionada = Funciones.devolverFigura(figuras, event.getX(), event.getY());
                    if (figSeleccionada == null) {}
                    else {
                        /*BUSCAMOS EL INDICE DE LA FIGURA*/
                        int valorFigura = Funciones.buscarIndice(figuras, figSeleccionada);
                        if (valorFigura >= 0) { 
                            /*DIBUJAMOS LA NUEVA FIGURA CON EL NUEVO COLOR*/
                            Group nuevoGroup = figSeleccionada.Dibujar(figSeleccionada.getX1(), figSeleccionada.getY1(), figSeleccionada.getNom(), colorpicker.getValue());
                            /*GUARDAMOS EL NUEVO COLOR DE ESTA FIGURA*/
                            if (figSeleccionada instanceof Entrasal) { colorEnt = colorpicker.getValue(); }
                            if (figSeleccionada instanceof Infin) { colorIni = colorpicker.getValue(); }
                            if (figSeleccionada instanceof Docu) { colorDoc = colorpicker.getValue(); }
                            if (figSeleccionada instanceof Proceso) { colorProc = colorpicker.getValue(); }
                            if (figSeleccionada instanceof Condicion) { colorCond = colorpicker.getValue(); }
                            /**************************************************/
                            /*LA REEMPLAZAMOS POR LA ANTERIOR*/
                            figSeleccionada.setNuevo(nuevoGroup);
                            /*LA REEMPLAZAMOS EN EL DIAGRAMA*/
                            figuras.set(valorFigura, figSeleccionada);
                            mostrarwas(); 
                            /*OBTENEMOS SU ID*/
                            String idFigSele = figSeleccionada.getNuevo().getId();              
                            for (int i = 0; i < figuras.size(); i++) {
                                /*OBTENEMOS LAS FIGURAS DEL DIAGRAMA*/
                                Figuras figuraDiagrama = figuras.get(i);
                                /*OBTENEMOS EL ID DE CADA FIGURA*/
                                String idFigura = figuraDiagrama.getNuevo().getId();
                                /*LO COMPARAMOS CON LA ULTIMA FIGURA PINTADA*/
                                if (idFigura.equals(idFigSele)) {
                                    /*OBTENEMOS EL INDICE DE LA FIGURA COINCIDENTE Y LA REEMPLAZAMOS*/
                                    valorFigura = Funciones.buscarIndice(figuras, figuraDiagrama);
                                    Group nuevoGroup2 = figuraDiagrama.Dibujar(figuraDiagrama.getX1(), figuraDiagrama.getY1(), figuraDiagrama.getNom(), colorpicker.getValue());
                                    figuraDiagrama.setNuevo(nuevoGroup2);
                                    figuras.set(valorFigura, figuraDiagrama);
                                    mostrarwas();                                    
                                }
                            }

                        }                        
                    }
                }
            }
        });
    }
    
    private Color colorEnt = Color.DARKRED;
    private Color colorDoc = Color.DARKSEAGREEN;
    private Color colorIni = Color.DARKCYAN;
    private Color colorProc = Color.DARKORANGE;
    private Color colorCond = Color.LIGHTSKYBLUE;
    private Color buscarColorFigura(String figura, Figuras nueva) {
        if (figura.equals("entrada")) {
            return colorEnt;
        }
        if (figura.equals("documento")) {
            return colorDoc;
        }
        if (figura.equals("inicio")) {
            return colorIni;
        }
        if (figura.equals("fin")) {
            return colorIni;
        }
        if (figura.equals("proceso")) {
            return colorProc;
        }
        if (figura.equals("condicion")) {
            return colorCond;
        }
        if (figura.equals("ciclo")) {
            return colorCond;
        }  
        /*NO SE SI ESTO SIRVE PERO LO DEJARE POR SI ACASO XD*/
        Color colorFig = nueva.getColorFig();
        for (int i = 0; i < figuras.size(); i++) {
            if (figuras.get(i).getNuevo().getId().equals(figura)) {
                System.out.println(figuras.get(i).getNuevo().getId());
                colorFig = figuras.get(i).getColorFig();
                return colorFig;
            }
        }
        return colorFig;
    }

    private void reemplazarFigura(String figuraCambio, int valorFiguraSeleccionada, Figuras nueva) {
        if ("ENTRADA".equals(figuraCambio)) {
            /*DIBUJAMOS LA NUEVA FIGURA*/
            Entrasal nuevaEntrada = new Entrasal();
            Group nuevoGroup = nuevaEntrada.Dibujar(nueva.getX1(), nueva.getY1(), nueva.getNom(), buscarColorFigura("entrada", nueva));
            /*LA REEMPLAZAMOS POR LA ANTERIOR*/
            nuevaEntrada.setNom(nueva.getNom());
            nuevaEntrada.setX1(nueva.getX1());
            nuevaEntrada.setY1(nueva.getY1());
            nuevaEntrada.setNuevo(nuevoGroup);
            /*LA REEMPLAZAMOS EN EL DIAGRAMA*/
            figuras.set(valorFiguraSeleccionada, nuevaEntrada);
            mostrarwas();
        }
        if ("PROCESO".equals(figuraCambio)) {
            /*DIBUJAMOS LA NUEVA FIGURA*/
            Proceso nuevoProceso = new Proceso();
            Group nuevoGroup = nuevoProceso.Dibujar(nueva.getX1(), nueva.getY1(), nueva.getNom(), buscarColorFigura("proceso", nueva));
            /*LA REEMPLAZAMOS POR LA ANTERIOR*/
            nuevoProceso.setNom(nueva.getNom());
            nuevoProceso.setX1(nueva.getX1());
            nuevoProceso.setY1(nueva.getY1());
            nuevoProceso.setNuevo(nuevoGroup);
            /*LA REEMPLAZAMOS EN EL DIAGRAMA*/
            figuras.set(valorFiguraSeleccionada, nuevoProceso);
            mostrarwas();
        }
        if ("DOCUMENTO".equals(figuraCambio)) {
            /*DIBUJAMOS LA NUEVA FIGURA*/
            Docu nuevoDoc = new Docu();
            Group nuevoGroup = nuevoDoc.Dibujar(nueva.getX1(), nueva.getY1(), nueva.getNom(), buscarColorFigura("documento", nueva));
            /*LA REEMPLAZAMOS POR LA ANTERIOR*/
            nuevoDoc.setNom(nueva.getNom());
            nuevoDoc.setX1(nueva.getX1());
            nuevoDoc.setY1(nueva.getY1());
            nuevoDoc.setNuevo(nuevoGroup);
            /*LA REEMPLAZAMOS EN EL DIAGRAMA*/
            figuras.set(valorFiguraSeleccionada, nuevoDoc);
            mostrarwas();
        }        
    }

    @FXML
    private void cambiarFigura(ActionEvent event) {
        mostrar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 1) {
                    /*BUSCAMOS LA FIGURA SELECCIONADA*/
                    Figuras figSeleccionada = Funciones.devolverFigura(figuras, event.getX(), event.getY());
                    /*VERIFICAMOS QUE NO ESTEMOS CAMBIANDO UN INICIO, UN FINAL, UN CICLO O UNA CONDICION*/
                    if (figSeleccionada instanceof Infin || figSeleccionada instanceof Condicion 
                       || figSeleccionada instanceof Ciclo) {
                        Alert alerta = new Alert(AlertType.ERROR);
                        alerta.setHeaderText("ERROR");
                        alerta.setContentText("RECUERDA QUE NO PUEDES CAMBIAR: UN INICIO, UN FINAL, UN CICLO O UNA CONDICION");
                        alerta.showAndWait();
                        return;
                    }
                    if (figSeleccionada == null) {}
                    else {
                        /*BUSCAMOS EL INDICE DE LA FIGURA*/
                        int valorFiguraSeleccionada = Funciones.buscarIndice(figuras, figSeleccionada); 
                        if (valorFiguraSeleccionada >= 0) {
                            /*VENTANA EMERGENTE QUE NOS AVISA PARA SELECCIONAR UNA NUEVA FIGURA*/
                            Stage theStage = FlowChart.getPrimaryStage();
                            List<String> figuraCambio = new ArrayList<>();
                            figuraCambio.add("ENTRADA");
                            figuraCambio.add("PROCESO");
                            figuraCambio.add("DOCUMENTO");
                            /*QUITAMOS LA MISMA ALTERNATIVA DE LA FIGURA SELECCIONADA*/
                            if (figSeleccionada instanceof Entrasal) {figuraCambio.remove(0);}
                            if (figSeleccionada instanceof Proceso) {figuraCambio.remove(1);}
                            if (figSeleccionada instanceof Docu) {figuraCambio.remove(2);}
                            /*********************************************************/
                            ChoiceDialog<String> dialog = new ChoiceDialog<>("ELIGE UNA FIGURA", figuraCambio);
                            dialog.setTitle("Cambiar figura");
                            dialog.setHeaderText("Seleccionar cambio");
                            dialog.setContentText(null);
                            Optional<String> result = dialog.showAndWait();                            
                            /***********************************************/
                            if (result.isPresent()) {
                                reemplazarFigura(result.get(), valorFiguraSeleccionada, figSeleccionada);
                                
                            }
                        }
                    }
                }                
            }            
        });
    }
    private boolean comprobardiagrama(){
        for (int i = 0; i < figuras.size(); i++) {
            if (i==0) {
                if (!(figuras.get(i)instanceof Infin) || figuras.get(i).isUnidoanterior() || !figuras.get(i).isUnidosiguiente()) {
                    return false;
                }
            }
            else if (i==figuras.size()-1) {
                if (!(figuras.get(i)instanceof Infin) || !figuras.get(i).isUnidoanterior() || figuras.get(i).isUnidosiguiente()) {
                    return false;
                }
            }
            else{
                if (figuras.get(i) instanceof Condicion) {
                    if (!figuras.get(i).isUnidoanterior() || !figuras.get(i).isUnidosiguiente()|| !((Condicion)figuras.get(i)).isUnidoder()) {
                        return false;
                    }                    
                }
                else if (figuras.get(i) instanceof Ciclo) {
                    if (!figuras.get(i).isUnidoanterior() || !figuras.get(i).isUnidosiguiente()|| !((Ciclo)figuras.get(i)).isUnidoizq()) {
                        return false;
                    }                    
                }
                else{
                    if (!figuras.get(i).isUnidoanterior() || !figuras.get(i).isUnidosiguiente()) {
                        return false;
                    }                    
                }
            }
        }
        return true;
    }
    
    private boolean verificarFinal(){
        for (int i = 0; i < figuras.size(); i++) {
            if (figuras.get(i) instanceof Infin) {
                if ("Final".equals(figuras.get(i).getNom())) {
                    return true;
                }
                
            }
        }
        return false;
    }
    
    private int verificarFinal2(){
        int valor=0;
        for (int i = 0; i < figuras.size(); i++) {
            if (figuras.get(i) instanceof Infin) {
                valor++;
                
            }
        }
        return valor;
    }
}
